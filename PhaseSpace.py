from .Axis import AxisSpec

import numexpr as ne
import numpy as np

class PhaseSpace:
    """Represents a grid of contiguous bins in a N dimensional phase space. 
    Each dimension is given by a AxisSpec.
    """

    base = None # non None if self is a subspace of an other PhaseSpace
    class Indices:
        """A helper class to conveniently obtain a block of index coordinates in a given PhaseSpace bps.
        
        usage is as in : coords = bps.indices[:2,:,4] 
        then coords is an array of (N, ndim). coords contains all possible index where 
          * dim 0 varies from 0 to 2
          * dim 1 varies on all possible index in  bps.axis[1]
          * dim 2 contains only index 4 
        """
        def __init__(self, bps):
            self.bps = bps

        def __getitem__(self, sl ):
            if sl==() or sl is None:
                sl=[ slice(None) for s in self.bps.axis ]
            if isinstance(sl, slice):
                sl = (sl,)
            indices = [ np.arange(a.nbins)[s]  for (a,s) in zip(self.bps.axis, sl) ]
            #print(indices)
            # trick to build a cartesian product of 'indices' :
            t = np.array(np.meshgrid( *indices ,indexing='ij') ).T.reshape(-1,self.bps.nDim())
            return t
        
    
    def __init__(self, name,  *axis):
        """Construct a PhaseSpace from a list of AxisSpec 
        """
        self.name = name
        if axis==():
            axis = []#[AxisSpec("Scalar",nbins=1,binNames=["0"])]
        self.axis = axis
        for i,a in enumerate(axis):
            setattr(self,'a'+str(i),a)
        self.tagBase = '_'.join( ['{:d}'] * len(axis) )
        self.nbins = 1
        for a in self.axis: self.nbins *= a.nbins
        self.indices = PhaseSpace.Indices(self)
        
    def shape(self):
        return tuple( a.nbins for a in self.axis)
        
    def nDim(self):
        return len(self.axis)
            
        
    def iter(self):
        # simple iterator over all possible indices.
        it =np.ndindex( tuple(a.nbins for a in self.axis)[::-1] )
        for i in it:
            yield i[::-1]
        
        
    def findBins(self, coords, indices=None,validity=None):
        """Find the multi-dim indices of points represented by coords.
        coords is in the form [array([x0,x1,x2,...,xN]), array([y1,y2,...,yN])...] or an array of shape (ndim,N)
        
        returns (indices, validity)  
        where 
          indices : tuple of ndim arrays of size N
          validity : array of bool of size N

        if given, indices and validity are filled in-place before being returned.
        """
        if indices is None:
            indices = np.zeros_like( coords, dtype=int)
        validityI = np.zeros_like( coords, dtype=bool)
        if validity is None:
            validity = np.empty( (coords[0].size, ), dtype=bool)
        
        # for each dimension find the index of the coordinates and if they are valid.
        #  put the results in each entry of indices and validityI
        for i,co in enumerate(coords):
            self.axis[i].binIndexAndValidity(co, indices[i], validityI[i] )

        # evaluate the validity for each point by conbining with AND the validity of each dimension.
        #   - build the expression in the form "i1&i2&i3..."
        nL = ['i'+str(i) for i in range(self.nDim()) ]
        expr='&'.join(nL)

        #   - evaluate the expressions on all points corresponding to coords
        ne.evaluate( expr, local_dict=dict( (n,validityI[i]) for i,n in enumerate(nL) ),out=validity)
        return indices, validity

    def findValidBins(self, coords, indices=None, returnFlatI=False,vetoed=None):
        """ given a list of coordinates 'coords' (an array of shape (ndim, N)), return only the valid bin indices (whereas findBins() returns also invalid bins together with validity flags).
        2 objects are returned :

          * a tuple of ndim arrays where array i has indices in dimension i. Each array has size validN where validN is the number of valid entries in 'coords' (i.e within the binspace).
          * an array of indices corresponding to valid entries  in 'coords', thus of size validN.
        
        """
        if indices is None:
            indices = np.zeros_like( coords, dtype=int)
        validity = np.empty( (coords[0].shape[0],)   , dtype =bool )
        self.findBins(coords, indices, validity )
        if vetoed is not None:
            validity &= vetoed

        #print('inputs ', coords[0][:5])
        #print('validity', validity[:5])

        # get indices that are non-zero (thus True) in validity 
        validCoordI = np.flatnonzero( validity)
        validN = len(validCoordI)
        for i in range(self.nDim()):
            # take only the valid indices for dim i , and put them back into indices 
            np.take(indices[i], validCoordI, out=indices[i][:validN])

        tupleI = tuple(i[:validN] for i in indices)

        if returnFlatI:            
            ii=np.ravel_multi_index(tupleI, self.shape() )
            return ii, validCoordI
        else:
            return tupleI, validCoordI
        

    def describeBin(self, bin ):
        if isinstance(bin,int):
            bin = np.unravel_index(bin, self.shape() ) 

        return [ a.describeBin(bin[i]) for (i,a) in enumerate(self.axis)]


    def __repr__(self):
        l=[f"PhaseSpace '{self.name}' ({self.nbins} bins in {self.nDim()} dimensions)"]
        t=np.get_printoptions()['threshold']
        np.set_printoptions(threshold=7) 
        l += [ f'{a.name:>20} : {a._compactdesc()}' for a in self.axis ]
        np.set_printoptions(threshold=t)
        return '\n'.join(l)
        

    def tagAt(self, coords):
        return self.tagBase.format(*coords)


    def emptyCoordinates(self, N, fill=None):
        c = np.empty( (self.nDim(), N)  , dtype =int )
        if fill is not None:
            c.fill(fill)
        return c


    def axisTag(self):
        return ''.join(a.name for a in self.axis)

    def subspace(self, retainDims=(), removeDim=() ):
        """Returns a new PhaseSpace representing a subspace of self. 
        Can be called either by passing the removed dimensions or the retained dimensions

        see also __getitem__ for an other way to create subspace by slicing.
        """
        if removeDim!= () and retainDims != ():
            print ("PhaseSpace.subspace() ERROR : specify either removeDim either retainDims, not both")
            raise
        if removeDim != ():
            if isinstance(removeDim,int): removeDim=(removeDim,)
            retainDims = tuple( i for i in range(self.nDim()) if i not in removeDim)

        newaxis = [self.axis[i] for i in retainDims]
        subps= PhaseSpace(self.name+'_'.join([str(i) for i in retainDims]), *newaxis)
        subps.base = self
        return subps

    def __getitem__(self, args):
        """returns a new PhaseSpace as a subspace of selg.
        args : a list of ndim slice specifications. 
               If an entry is the object rm from this module, the corresponding dim is excluded.
               Else each entry is passed to AxisSpec.subaxes() to slice the corresponding dim.
        ex : 
          # ps has shape (3,5,10) 
          subps = ps[rm,:,7:]
          # --> subps has shape (5,3) 
        """
        global rm
        newaxes = []
        for i,s in enumerate(args):
            if s is rm or isinstance(s,int):
                # if a dim is removed or just a single bin, we ignore it
                continue
            newaxes.append( self.axis[i].subaxis( s ) )
        subps = PhaseSpace(self.name+"_", *newaxes)
        subps.base  = self
        return subps




    def sliceWithSum(self, args):
        """mostly for internal uses. This interprets args as a slice of this phase space, allowing dimension of the slice to be sum
        Returns a tuple of 3 (p, s, dimtobesummed)
          - the corresponding sliced PhaseSpace : p
          - a slice s to be taken in arrays with shape compatible with self
          - a list of dim to be summed in order to realize the 'args' slice: if a is an array,  'a[args]' is actually realized by 'a[s].sum(dimtobesummed)' 
        """
        dimtobsummed = []
        arrayslices = []
        psslices = []
        if not isinstance(args,tuple):
            args=(args,)
        # format the inputs in proper slice and detected dimensions to be summed
        global rm
        currentOutDim=0 # count the output dim (because when slicing with an integer -> 1 less dimension in the output)
        for i,s in enumerate(args):
            if isinstance(s,int) :
                ar_=s
                ps_= rm
            elif s is sum:
                ar_=slice(None)
                dimtobsummed.append( currentOutDim )
                ps_= rm
                currentOutDim+=1
            elif s.step is sum:
                ar_ = slice(s.start, s.stop, None)
                ps_ = rm
                dimtobsummed.append( currentOutDim )
                currentOutDim+=1
            elif s.step is not None:
                raise Exception("AxisSpec slicing with stride not supported")
            else:
                ar_=s
                ps_=s
                currentOutDim+=1
            arrayslices.append( ar_ )
            psslices.append( ps_ )
            
        arrayslices = tuple(arrayslices)
        dimtobsummed = tuple( sorted(dimtobsummed))
        bps = self[psslices]
        return bps, arrayslices, dimtobsummed
    
    
class RemoveDim:
    pass
rm = RemoveDim()
