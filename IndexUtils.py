import numpy as np
import numba

@numba.njit(parallel=True)
def binIndexAndValidtyGeneric(bins, x, indices, validity):
    """Calculate the bin index of 'x' values corresponding to bins defined by 'bins'
    fill these in 'indices' and wether they are valid in 'validity' (array of bool)
    """
    indices[:] = np.searchsorted(bins,x, 'right')

    n = bins.size-1
    for i in numba.prange(x.size):
        indices[i] -= 1 
        validity[i]= (indices[i]>-1)and(indices[i]<n)
        

@numba.njit(parallel=True)
def binIndexAndValidityReg(nbin, xmin, xmax, x, indices, validity):
    
    s = nbin / (xmax - xmin)
    for i in numba.prange(x.size):
        u=int(np.floor( (x[i]-xmin)*s ))
        indices[i]= u
        validity[i]= (u>-1)and(u<nbin)
        

@numba.njit(parallel=True)
def binIndexReg(nbin, xmin, xmax,x, indices):
    """Return the indices of bins in which x fall, assuming there are  'nbin' bins between  xmin and xmax AND overflow and underflow bins.
    Underflows are indexed at 0, overflows indexed at nbin+1. 
    Valid bins are thus in [1, nbin]
    """
    s = nbin / (xmax - xmin)
    for i in numba.prange(x.size):
        # perform (x - xmin)*s +1
        u = numba.int64( (x[i]-xmin)*s+1)
        # clip to 0 and nbins
        u = u if u>0 else 0
        indices[i]= u if u <(nbin+1) else nbin+1
    return indices



def _addat(target, index, tobeadded):
    raise Exception
    #return np.add.at(target, index, tobeadded)

@numba.extending.overload(_addat)
def ol_addat(target, index, tobeadded):
    """Equivalent of np.add.at(target, index, tobeadded) but much faster. """
    def addat_scalar(target, index, tobeadded):
        for i in range( index.size):        
            target[index[i]] += tobeadded
    def addat_vector(target, index, tobeadded):
        for i in range( index.size):        
            target[index[i]] += tobeadded[i]
    if isinstance(tobeadded, numba.types.Number):
        return addat_scalar
    else:
        return addat_vector

@numba.njit
def addat(target, index, tobeadded):
    return _addat(target, index, tobeadded)
    
    
@numba.njit(parallel=True)
def fullIndices(innerInd, outerSize, outerInd):
    """Equivalent innerInd*outerSize+outerInd but much (?) faster. """
    for i in numba.prange(outerInd.size):
        outerInd[i] = innerInd[i]*outerSize+outerInd[i]
    return outerInd
    
