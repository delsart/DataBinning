import numpy as np

from .Axis import AxisSpec
from .BinContainer import BinContainer
from .Histogram import Histo1D, Histo2D
from .IndexUtils import fullIndices, addat

@BinContainer.toKnownClasses
class NumberContainer(BinContainer):

    arrays = []
    narray = 0
    def __init__(self, name, phspace,  initArrays=True, ):
        self.arrays = []
        super().__init__(name, phspace,  initArrays,)

    def initArrays(self):
        shape = [a.nbins for a in self.phspace.axis]
        self.arrays = []
        for i in range(self.narray):
            self.arrays.append( np.zeros( shape , dtype='float32') )



    def __getitem__(self, args):
        """Support for slicing with the operator []. Let bs is a BinnedScalar, then 
          bs[:3,1:4]   is a BinnedScalar containing the rectangle from index (0,1) to  (2,3) from bs (similar as slicing a numpy array)
          bs[sum,1:4]  is a BinnedScalar with only the bins 1 to 3 in the 2nd dimension, and each containing the sum of entries in 1st dimension
          that is : bs[sum, a:b][i] = sum_over_k( bs[k,i+a] ) 
          also possible :
          bs[1:4:sum, a:b] (sum between 1 up to 3) 
          bs[:4:sum, a:b] (sum from 0 up to 3) 
          bs[1::sum, a:b] (sum from 1 up to max) 

        IMPORTANT : this uses numpy slices, so the array of the returned BinnedScalar is shared with its parent whenever possible.
                    when summing the returned BinnedScalar always contains different arrays. 
        """
        # retrieve the sliced histos and which bin/dimensions to sum
        phspace, finalslices, dimtobsummed = self.phspace.sliceWithSum( args)

        if self.arrays == [] :
            self.loadContent()
            
        # the new BinnedScalar
        bs = self.__class__( self.name+'__',phspace, initArrays=False)
        bs.arrays = []
        for a in self.arrays:
            bs.arrays.append( a[finalslices] )

        # replace by sum allong dims if needed.
        if len(dimtobsummed)>0:
            for i in range(self.narray):
                bs.arrays[i] = bs.arrays[i].sum( dimtobsummed)

        bs.setup()
        #bs.drawStyle = self.drawStyle
        return bs
        
    def sumAlong(self, dim):
        bs = self.__class__( self.name+f'_sum{self.phspace.axis[dim].name}',
                             self.phspace.subspace(removeDim=(dim,) ),initArrays=False )
        bs.arrays =[ a.sum(dim) for a in self.arrays]
        bs.setup()
        
        return bs

    @staticmethod
    def concat(listBS, name=None):
        """create a new BinnedScalar by stacking the BinnedScalar in listBS. 
        All PHSPACE in listBS must be the same (phspace0), then the new BinnedScalar has a new PHSPACE being phspace0+1 dim
        """
        bs0 = listBS[0]
        phspace = PhaseSpace( bs0.phspace.name+"_list", *(bs0.phspace.axis+(AxisSpec('n', len(listBS) , range=(0,len(listBS)) ),)  ) )
        name = name or  'concat_'+bs0.name
        newBS = self.__class__( name,phspace, initArrays=True)
        for i,bs in enumerate(listBS):
            for k in range(self.narray):                
                newBS.arrays[k][...,i] = bs.arrays[k]
        return newBS
        
            
            
@BinContainer.toKnownClasses
class ScalarContainer(NumberContainer):
    """Store 1 number per bin in a multi-dimension binned phase space"""
    # what members will be persistified
    persistentAttr = []
    persistentAttrHeavy = [ 'values']
    persPrefix = 'ScC'

    narray = 1
    def __init__(self, name, phspace,  initArrays=True, ):
        """ phspace: the PhaseSpace representing the bins of the phase space in which the scalar values are kept
        """
        super().__init__(name, phspace,  initArrays,)
        if phspace: self.setup()


    def setup(self):
        if self.arrays!=[]:
            # we're created from scratch self.arrays is our content, just name it as an attribute
            self.values = self.arrays[0]

    def loadContent(self,f=None):
        super.loadContent(f)
        # we're created from file : our attributes are already set, just link them from arrays
        self.arrays = [ self.values ] 

        
@BinContainer.toKnownClasses
class StatContainer(NumberContainer):
    """Store a weighted count  per bin in a multi-dimension binned phase space 
    --> This is class actually represents a N-dim histogram (& profile) without under/overflow !
    """
    
    # what members will be persistified
    persistentAttr = ['nentries']
    persistentAttrHeavy = [ 'values', 'values2','sumw', 'sumw2']
    persPrefix = 'StC'

    narray = 4
    def __init__(self, name, phspace,  initArrays=True, ):
        """ phspace: the PhaseSpace representing the bins of the phase space in which the scalar values are kept
        """
        super().__init__(name, phspace,  initArrays,)
        self.nentries = 0
        if phspace: self.setup()


    def setup(self):
        if self.arrays!=[]:
            # we're created from scratch self.arrays is our content, just name it as an attribute
            self.values = self.arrays[0]
            self.values = self.arrays[1]
            self.sumw = self.arrays[2]
            self.sumw2 = self.arrays[3]

    def loadContent(self,f=None):
        super.loadContent(f)
        # we're created from file : our attributes are already set, just link them from arrays
        self.arrays = [ self.values,self.values2,self.sumw, self.sumw2 ] 
        


    def mean(self):
        return np.where( self.sumw>0 , self.values/self.sumw, 0)

    def meanAndErr(self):
        mean = np.where( self.sumw>0 , self.values/self.sumw, 0)
        over_neff =  np.where( self.sumw>0, self.sumw2 / self.sumw**2, 0.)
        err = np.sqrt( ( self.values2/self.sumw -mean**2 )*over_neff)

        return mean ,err

    

    def accumulate(self, coords, values, weights=1.):
        """Accumulate values in each bins according to their coords.
           coords = array( (ndim, nvalues), dtype=float32)
           values = array( (nvalues,), dtype=float32) 

        this is equivalent to 'self.values[ coordsIndex ]+=values*weights'

        """
        # find indices of coords falling in valid nbin :
        tupleI,validCoordI = self.phspace.findValidBins(coords ,returnFlatI=True )
        # validCoordI is the indices of values corresponding to valid bins
        if isinstance(weights, np.ndarray):
            weights = weights[ validCoordI ]
        
        self.accumulateAt( tupleI, values[validCoordI], weights )

    def accumulateAt(self, coordInd, values, weights=1):
        """Accumulate values at indices given by coordInd 
           coords = array( (nvalues,) dtype=int) or (array( (nvalues,), ..., array( (nvalues,) ) 
           values = array( (nvalues,), dtype=float32) 

        this is equivalent to 'self.values[ coordInd ]+=values*weights'
        """
        if isinstance(coordInd, tuple):
            # then validTupleIndices=(ind_0,ind_1,...) where ind_i is an array of indices in dimension i
            coordInd=np.ravel_multi_index( coordInd, self.phspace.shape() )

        addat(self.values.ravel(), coordInd, values*weights)            
        addat(self.values.ravel(), coordInd, values*values*weights)            
        addat(self.sumw.ravel(), coordInd, weights)            
        addat(self.sumw2.ravel(), coordInd, weights*weights)            

        if isinstance(weights, np.ndarray):
            self.nentries += weights.sum()
        else:
            self.nentries += coordInd.size
        
