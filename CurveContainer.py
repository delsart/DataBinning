import numpy as np

from .Axis import AxisSpec
from .BinContainer import BinContainer
from .IndexUtils import fullIndices, addat



@BinContainer.toKnownClasses
class CurveContainer(BinContainer):
    """Represents a set of 1D curves arranged in a multi-dimension binned phase space"""

    persistentAttr = ['maxN', 'xlabel', 'ylabel']
    persistentAttrHeavy = [ 'array_x', 'array_y', 'array_yerr', 'npoints']
    persPrefix = 'CuC'

    array_x = None
    array_y = None
    numInnerDim = 1 # but there 2 arrays
    def __init__(self, name, bps, maxN=None, withErr = True, initArrays=True, xlabel='x', ylabel='y' ):
        """ bps: the BinnedPhaseSpace representing the bins of the phase space in which the histos are kept.
             """
        self.maxN = maxN
        self.withErr = withErr
        self.xlabel=xlabel
        self.ylabel = ylabel
        super().__init__(name, bps,  initArrays,)

        
    def initArrays(self):                         
        shape = [a.nbins for a in self.phspace.axis]+[self.maxN] # +2 : overflow and underflow bins 
        self.array_x = np.zeros( shape , dtype='float32')
        self.array_y = np.zeros( shape , dtype='float32')
        if self.withErr:
            self.array_yerr = np.zeros( shape , dtype='float32')
        self.npoints = np.ones( shape[:-1] ,dtype=int)*self.maxN




    def __getitem__(self, args):
        """Support for slicing with the operator []. Let bh is a BinnedHistos, then 
         (see BinnedHistos.__getitem__ for doc)
      
         UNTESTED yet ...
        IMPORTANT : this uses numpy slices, so the array of the returned BinnedHistos is shared with its parent whenever possible.
                    when summing the returned BinnedHistos always contains different arrays. 
        """

        print(" WARNING CurveContainer slicing untested yet !")
        # retrieve the sliced histos and which bin/dimensions to sum
        phspace, finalslices, dimtobsummed = self.phspace.sliceWithSum(args) #phspaceFromSliceSum(self.phspace, args)


        if len(dimtobsummed)>0:
            raise Exception("CurveContainer : slicing with sum not supported")

        if self.array is None:
            self.loadContent()
        
        # the new BinnedHistos
        bh = CurveContainer( self.name+'__',bps, maxN=self.maxN,
                             withErr=self.withErr, xlabel=self.xlabel, ylabel=self.ylabel, initArrays=False)

        allArrays = ['array_y', 'array_y','npoints'] + self.auxArrayNames
        if self.withErr:
            allArrays.append('array_yerr')
        # perform sliciing on all arrays :
        for aname in allArrays:            
            setattr(targetCont, aname,
                    getattr(self,aname)[ finalslices ] )
        
        bh.drawStyle = self.drawStyle
            
        return bh
