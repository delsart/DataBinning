# Data Binning

A library to manipulate data in a multi-dimensionnal binned phase space.
This library is pure python, based on numpy, and provide data analysis objects such as 
 - Histogram objects (1D and 2D)
 - Binned phase space description in arbitrary dimension (`PhaseSpace` object)
 - "Bin" containers in arbitrary dimension where "bin" can contain histo (ex: `Histo1DContainer`), curves or simple numbers.
 - Support slicing of phase spaces and containers as much is possible.
 - Graphic extensions for the base class, cleanly separated from the core objects.
 
 

## Exemple, Getting started

### clone

### Example

```python
from DataBinning.PhaseSpace import PhaseSpace, rm
from DataBinning.Axis import AxisSpec
from DataBinning.HistoContainer import Histo1DContainer,Histo2DContainer
from DataBinning.BinContainer import BinContainer

# create a phase space with 3 dimensions 
ps  = PhaseSpace("PS",
	# 1st dim : regular axis, by specifying nbins and range 
	AxisSpec("R", title="radius", nbins=20, range=(0., 3.5) ), 
	# 2nd dim : generic axis by specifiying the contiguous edges of the bins 
	AxisSpec("S", title="speed",edges=np.array([0,10, 15, 20, 60]) ),
	# 3rd dim : integral or categorical axis
	AxisSpec("C", title="color", binNames= ["red","green","black"] ),
)

# next create a container of histo :

ipCont = Histo1DContainer("impact", ps, hspec=(100, (-2,2) ) )


```
