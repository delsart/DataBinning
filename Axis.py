import numpy as np
import numexpr as ne
from .IndexUtils import binIndexAndValidityReg, binIndexAndValidtyGeneric, binIndexReg

class AxisException(Exception):
    pass

class AxisSpec:
    """Represents bins in 1 dimension. 
    This is essentially a list of bins on a 1D line which can be defined either by 
      * nbins , (xmin, xmax)  --> regular bins 
      * the list of nbins+1 edges of the bins. 
    In both cases we have : edges[0]=xmin and edges[nbins]=xmax 

    2 conventions of indexing are supported when retrieving bin indices :
     - 'rawbin' (default) : indices are simply in [0, nbins-1] and can directly refer to an entry in a numpy array
     - 'histobin' : indices are in [1,nbins] allowing client to use 0/nbins+1 to index under/overflow bins in a histogram (as in ROOT::TH1).
                    methods use this convention if passed the histobin=True arg or are named explicitly with 'histobin'

    """
    
    save_att = ['edges', 'isRegular','name', 'title', 'isIntBin', 'binNames']

    base =None
    binNames = None
    
    def __init__(self, name, nbins=None, range=None, edges=None, title=None, binNames=None,
                 binDescriber=None, log=False,
                 **args):
        self.name = name
        self.title = title or name

        
        if edges is not None:
            edges = np.array(edges)
            d0 = (edges[1]-edges[0])
            self.isRegular = np.all( np.abs(edges[1:]-edges[:-1] -d0) < d0*1e-3  )
            if nbins or range is not None :
                print("AxisSpec constructor ERROR specifiy either edge  either  (nbins and range) ")
                raise
            self.edges = edges
            self.nbins = len(edges)-1
            self.range = edges[0], edges[-1]
            self.isIntBin = False
        elif range is not None:
            self.isRegular = True
            if edges is not None:
                raise AxisException("AxisSpec constructor ERROR specifiy either edge eithe nbins and range")
            if nbins is None:
                raise AxisException("AxisSpec constructor ERROR : range specified but nbins is None")
            self.nbins = nbins
            self.range = range
            if log :
                range = np.log10(range)
                self.edges = np.logspace( range[0], range[1], nbins+1)
                self.isRegular = False
            else:                
                self.edges = np.linspace(range[0], range[1],nbins+1)
            self.isIntBin = False
            
        elif nbins is not None:
            self.edges = np.arange(nbins+1)
            self.nbins = nbins
            self.range = self.edges[0], self.edges[-1]
            self.isIntBin = True
            self.isRegular = True
        elif binNames is not None:
            self.nbins = len(binNames)
            self.edges = np.arange(self.nbins+1)
            self.range = self.edges[0], self.edges[-1]
            self.isIntBin = True
            self.isRegular = True            
        else:
            self.isIntBin = False
            self.isRegular = False
            self.nbins = 0

        if binDescriber is None:
            self._set_binDescriber()
        else:
            self.binDescriber = binDescriber

        if binNames is not None:
            self.binNames = binNames
        for k,v in args.items():
            setattr(self,k,v)
        self._custom_args = list(args.keys())

    def clone(self, **args):
        newa = AxisSpec(self.name, title = self.title)
        newa._custom_args = self._custom_args
        for a in newa._custom_args+self.save_att+['nbins','range','base']:
            setattr(newa,a,getattr(self,a))
        return newa
            
    def binIndexAndValidity(self,x, indices=None, validity=None):
        """Returns (indices, validity) where 
            - indices contain indices of bins to which x values belong
            - validity is True or False depending if x is in the range or not,
        
        (default 'rawbin' convention : returned valid index are in [0,nbins-1] )
        """
        from numbers import Number
        scalar = isinstance(x,Number)
        if scalar:
            x=np.array([x])

        if validity is None:
            validity = np.empty(x.size, dtype=np.bool_)
        if indices is None:
            indices = np.empty(x.size,dtype=np.int64)
        
        if self.isRegular:
            binIndexAndValidityReg(self.nbins, self.range[0],self.range[1], x, indices, validity)
        else:
            binIndexAndValidtyGeneric(self.edges, x, indices, validity)

        if scalar:
            return indices[0], validity[0]
        return indices,validity

    def histobinIndex(self, x, indices=None):
        """Returns the indices of bins to which x values belong.
        IMPORTANT, 'histobin' convention : valid index are in [1,nbins] 
        """
        from numbers import Number
        scalar = isinstance(x,Number)
        if scalar:
            x=np.array([x])
        if self.isRegular:
            if indices is None:
                indices = np.zeros(x.size,dtype=np.int64)            
            binIndexReg(self.nbins, self.range[0], self.range[1], x, indices)
        else:
            if indices:
                indices[:] = np.searchsorted(self.edges,x, 'right')
            else:
                indices = np.searchsorted(self.edges,x, 'right')
        if scalar:
            return indices[0]
        return indices

    def binCenter(self, i=None, histobin=False):
        if i is None:
            return 0.5*(self.edges[:-1]+self.edges[1:])
        if histobin:
            return 0.5*(self.edges[i-1]+self.edges[i])
        else:
            return 0.5*(self.edges[i+1]+self.edges[i])

    def describeBin(self, i):
        """IMPORTANT 'rawbin' convention """        
        return self.binDescriber(self, i)

    def binWidth(self, i=0, histobin=False):
        if histobin:
            return self.edges[i] -self.edges[i-1]
        else:
            return self.edges[i+1] -self.edges[i] 

    def dump(self):
        print( self.name, self.range, self.edges)

    def _desc(self):
        if self.isIntBin:
            htype = 'Integer'
            if self.binNames is not None:
                suffix = f"bin names={self.binNames[:2]}"
                if len(self.binNames)>2: suffix = suffix[:-1]+',...]'
            else:
                suffix='bin names= []'
        elif self.isRegular:
            htype = 'Regular'
            suffix = f' range={self.range}'
        else:
            htype = 'Generic'
            suffix = f' edges={self.edges}'
        return f"{htype} axis, nbins={self.nbins}"+suffix

    def _compactdesc(self):
        if self.isIntBin:
            if self.binNames is not None:
                suffix = f'{self.binNames[:2]}'
                if len(self.binNames)>2: suffix = suffix[:-1]+',...]'
            else:
                suffix='[]'
            return f'nbins={self.nbins}, {suffix}'
        elif self.isRegular:
            return f'nbins={self.nbins}, {self.range}'
        else:
            return f'nbins={self.nbins}, {self.edges}'
        
    
    def __repr__(self):
        return f"Axis {self.name} : "+ self._desc()
        
    def saveTo(self, out, prefix=None):
        prefix= prefix or self.name+"_"
        for att in self.save_att:
            out[prefix+att] = getattr(self, att)
    
    def loadFrom(self, inp, prefix=None):
        prefix= prefix if prefix is not None else self.name+"_"
        for att in self.save_att:
            setattr(self, att, inp.get(prefix+att,"") )

        e = self.edges
        self.range = (e.min(), e.max() )
        self.nbins = len(e)-1

        self._set_binDescriber()
        
    def _set_binDescriber(self):
        if self.isIntBin and self.binNames is not None:
            binDescriber = AxisSpec.describeName
        elif self.isIntBin:
            binDescriber = AxisSpec.describeIndex
        else:
            binDescriber = AxisSpec.describeInterval
        self.binDescriber = binDescriber

            
        

    def nameIndex(self, i ):
        if self.binNames is None:
            raise AxisException(f"Requesting bin '{i}' but Axis '{self.name}' does not have named bins")
        return self.binNames.index(i)


    def __len__(self):
        return self.nbins
    
    def __getitem__(self, i):
        """Returns bin edgeges or bin index. Follows the 'rawbin' convention"""
        if isinstance(i, str):
            i = self.nameIndex(i)

        if self.isIntBin:
            if i<0 or i>=self.nbins:
                raise AxisException(f"Axis {self.fname} : attempt to acces out-of-range bin {i} (max is {self.nbins})")
            return i

        return self.edges[i], self.edges[i+1]
        
    def subaxis(self,s):
        if isinstance(s,int) : s=slice(s,s+1)

        start, stop, step = s.indices(self.nbins)

        if (stop-start)%step !=0 :
            raise AxisException(f"Can't create rebinned axis from {start} to {stop} by {step}")
        
        newa = AxisSpec(self.name, title = self.title)
        newa.isRegular = self.isRegular
        newa.edges = self.edges[start:stop+1:step]
        newa.range = newa.edges[0], newa.edges[-1]
        newa.nbins = len(newa.edges)-1
        newa.isRegular = self.isRegular
        newa.isIntBin = False
        if self.binNames:
            newa.binNames = self.binNames[start:stop+1:step]
        if step==1:
            newa.base = self
        return newa


    @staticmethod
    def describeInterval(ax, i):
        low, up = ax[i]
        return f"{ax.title} $\\in [{low:.2f}, {up:.2f}]$"

    @staticmethod
    def describeIntervalI(ax, i):
        low, up = ax[i]
        return f"{ax.title} $\\in [{int(low)}, {int(up)}]$"
    
    @staticmethod
    def describeName(ax, i):
        return f"{ax.title} = '{ax.binNames[i]}'"

    @staticmethod
    def describeIndex(ax, i):
        return f"{ax.title} = {i}"
    
