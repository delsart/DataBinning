import numpy as np

from .Axis import AxisSpec
from .BinContainer import BinContainer
from .Histogram import Histo1D, Histo2D
from .IndexUtils import fullIndices, addat

@BinContainer.toKnownClasses
class Histo1DContainer(BinContainer):
    """Represents a set of histograms arranged in a multi-dimension binned phase space"""

    # what members will be persistified
    persistentAttr = ['xaxis_s'] # axis_s is a temporary member set during saving/loading
    persistentAttrHeavy = [ 'array', 'arrayw2', 'entriesPerH']
    persistPrefix = 'H1C'

    array = None
    xaxis = None
    numInnerDim = 1
    def __init__(self, name, phspace, hspec=None, initArrays=True, ):
        """ phspace: the PhaseSpace representing the bins of the phase space in which the histos are kept.
            hspec:  in the form (histo_nbins, (histo_min, histo_max) ) or (histo_nbins, (histo_min, histo_max), "name:title" )"""
        if isinstance(hspec, AxisSpec):
            self.xaxis = hspec
            self.nbins = hspec.nbins
            self.range = hspec.range
        elif hspec is not None:
            if len(hspec)==2 : hspec=tuple(hspec)+('xaxis:xaxis',)
            nbins, range, hname = hspec
            hname = hname.split(':')
            self.nbins = nbins
            self.range = range
            self.xaxis = AxisSpec( hname[0], nbins=nbins, range=range,title=hname[-1])
        else:
            self.xaxis = AxisSpec('xaxis')

        BinContainer.__init__(self,name, phspace,  initArrays,)
        self.persistNonArrays = []

        if phspace: self.setup()
                    

    def setup(self):
        BinContainer.setup(self)
        #if hasattr(self, 'bins'): self.nbins = self.bins # TEMPORARY to read back older version
        if self.xaxis :
            range = self.xaxis.range
            self.binwidth = (range[1]-range[0])/self.xaxis.nbins

    def initArrays(self):
        shape = [a.nbins for a in self.phspace.axis]+[self.xaxis.nbins+2] # +2 : overflow and underflow nbin 
        self.array = np.zeros( shape , dtype='float32')
        self.arrayw2 = np.zeros( shape, dtype='float32' )
        self.entriesPerH = np.zeros( shape[:-1] ,dtype=int)


    def loadFromH5(self, fname, loadContent=True, encoding=None):
        # load all attributes
        BinContainer.loadFromH5(self, fname, loadContent, encoding)

        # get back xaxis  from self
        from pickle import loads
        self.xaxis=loads(self.xaxis_s.tobytes())
        del(self.xaxis_s)
            
    def saveInH5(self, fname=None, mode='a', ):
        from pickle import dumps
        self.xaxis_s=np.bytes_(dumps(self.xaxis))
        BinContainer.saveInH5(self, fname, mode)
        # cleanup ourselves
        del(self.xaxis_s)

    def projectOnDim(self, i):
        newBh = Histo1DContainer( self.name+str(i), hspec=(self.xaxis.nbins, self.xaxis.range) ,phspace = self.phspace.subspace(removeDim=i) ,initArrays=False)
        newBh.array = self.array.sum(axis=i)
        newBh.arrayw2 = self.arrayw2.sum(axis=i)
        newBh.entriesPerH = self.entriesPerH.sum(axis=i)
        newBh.isLoadedFromInput =True
        return newBh

    def histNbins(self,):
        # +2 is for the under &overflow bins
        return self.xaxis.nbins+2
    
    ## ********************************
    # Filling methods
    def fill(self, coords, values, weights=1.):
        """Fill histos in nbin given by coords with values.
           coords = array( (ndim, nvalues), dtype=float32)
           values = array( (nvalues,), dtype=float32) 
        """
        # allocate arrays for indices        
        indices = np.zeros( (self.phspace.nDim()+1, values.shape[0])  , dtype =int )
        
        # find indices of coords falling in valid nbin :
        tupleI,validCoordI = self.phspace.findValidBins(coords,indices[:-1,:] , returnFlatI=True)
        # validCoordI is the indices of values corresponding to valid bins

        # prepare weights corresponding to valid bins 
        if isinstance(weights, np.ndarray):
            weights = weights[validCoordI]

        # finalize fill operations (this will compute histo indices and actually append values to histos)
        self.fillAtIndices( values,  validCoordI, tupleI,weights, weights*weights, indices[-1] )
        


    def fillAtIndices(self,values, validValueIndices,  validTupleIndices,  validW=1., validW2=None, hIndices=None):
        """Fill values into our histos, using a given pre-calculated list of valid indices (validValueIndices) and a corresponding list valid of bins (validTupleIndices, the bins index coordinates).         

        Optionnally fill with weights and squared weights.

        The input arrays must be like :
        validValueIndices = array( (validN,) )
        validTupleIndices = tuple( array(validN),  .. , array(validN) ) or array(validN) (in which case it's the array of indices along the flatten self.array )
        value, validW and validW2 = array( (N,) ) where N>= validN
        
        validValueIndices,  validTupleIndices are typically obtained from 'phspace.findValidBins( some_coords )'

        hIndices can be given to avoid allocation here. IT must be array( len(values), dtype=int) 
        """
        # map the values to their bin index into hIndices
        hIndices=self.histBinIndex(values, out=hIndices)

            
        validN =len(validValueIndices)
        print(validN, hIndices.shape, hIndices.dtype)
        # select only the histo entries corresponding to valid bins. 
        np.take(hIndices, validValueIndices, out=hIndices[:validN])

        if isinstance(validTupleIndices, tuple):
            # then validTupleIndices=(ind_0,ind_1,...) where ind_i is an array of indices in dimension i
            tupleI = validTupleIndices+(hIndices[:validN],)            
            ii=np.ravel_multi_index( tupleI, self.phspace.shape()+(self.histNbins(),) )
            np.add.at(self.entriesPerH, tupleI[:-1], 1)            
        else:
            # we are given flatten indices in the phase space. convert to bin position in histos :
            fullIndices(validTupleIndices, self.histNbins(), hIndices[:validN])
            ii = hIndices[:validN]            
            addat(self.entriesPerH.ravel(), validTupleIndices, 1) 
            
        if validW2 is None:
            validW2 = validW*validW            
        #  ravel() returns a flatten view of the array
        addat(self.array.ravel(), ii, validW)
        addat(self.arrayw2.ravel(), ii, validW2)
        

    def histBinIndex(self,values, out=None, flatInd=True):
        """Returns the indices of values in the  histogram of this Histo1DContainer
        """
        hIndices = out
        if hIndices is None:
            hIndices = np.zeros( values.size, dtype=int)
        self.xaxis.histobinIndex( values, hIndices)
        return hIndices
        

    ## ********************************

    def histoAt(self, coords):
        hcontent = self.array[coords]
        hname=self.nameAt( coords )
        h = Histo1D(name=hname, xaxis=self.xaxis,
                    wcounts  = hcontent,
                    w2counts = self.arrayw2[coords],
                    title = self.label,
                    )
        return h
        
    def drawAt(self, coords, ax=None, **opts):
        from matplotlib import pyplot as plt
        if ax is None: ax=plt.gca()
        if self.array is None:self.loadContent()
        h = self.histoAt(coords)
        return h.draw(ax=ax,**opts)

        

    # def saveAsROOT(self, fname):
    #     if isinstance(fname,str):
    #         f = uproot.recreate(fname)
    #     else: f= fname
        
    #     for index in np.ndindex( self.array.shape[:-1] ):
    #         #print(index)
    #         h = self.histoAt( index )
    #         f[h.name] = h
    #     f.close()

    def normalize(self, perHEntries=False):
        
        s = self.array.sum(self.phspace.nDim(), keepdims=True)
        if perHEntries: s /= self.entriesPerH.reshape( self.phspace.shape()+(1,) )
        try:
            test = self.sum_array 
            print(self.name, "already normalized")
        except:
            print( self.name , 'normalize')
            self.sum_array = s
        
        #if s==0: return
        self.array /= s
        self.arrayw2 /= s*s

    def renormalize(self,):
        self.array *= self.sum_array
        self.arrayw2 *= self.sum_array*self.sum_array



    def __getitem__(self, args):
        """Support for slicing with the operator []. Let bh is a Histo1DContainer, then 
          bh[:3,1:4]   is a Histo1DContainer containing the histos in the rectangle from index (0,1) to  (2,3) from the bs phase space
          bh[sum,1:4]  is a Histo1DContainer with only the bins 1 to 3 in the 2nd dimension, and each containing the sum of histos in 1st dimension
          that is : bh[sum, a:b][i] = sum_over_k( bh[k,i+a] ) 
          also possible :
          bh[1:4:sum, a:b] (sum between 1 up to 3) 
          bh[:4:sum, a:b] (sum from 0 up to 3) 
          bh[1::sum, a:b] (sum from 1 up to max) 

        IMPORTANT : this uses numpy slices, so the array of the returned Histo1DContainer is shared with its parent whenever possible.
                    when summing the returned Histo1DContainer always contains different arrays. 
        """

        # retrieve the sliced histos and which bin/dimensions to sum
        phspace, finalslices, dimtobsummed = self.phspace.sliceWithSum(args) #phspaceFromSliceSum(self.phspace, args)

        if self.array is None:
            self.loadContent()
        
        # the new Histo1DContainer
        bh = Histo1DContainer( self.name+'__',phspace, hspec=self.xaxis, initArrays=False)

        # assign bh's arrays
        self._slicearrays(bh, finalslices, dimtobsummed)

        if phspace.nbins==1:
            return bh.histoAt( (0,))

        return bh

    def _slicearrays(self,targetCont,finalslices, dimtobsummed, ):
        """ slice all our arrays into those of targetCont """
        allArrays = ['array', 'arrayw2','entriesPerH' ] + self.auxArrayNames
        # perform sliciing on all arrays :
        for aname in allArrays:            
            setattr(targetCont, aname,
                    getattr(self,aname)[ finalslices ] )


        # sum allong dims if needed.
        if len(dimtobsummed)>0:
            for aname in allArrays:
                
                setattr(targetCont, aname,
                        getattr(targetCont, aname).sum( dimtobsummed) )

        if targetCont.array.ndim == self.numInnerDim:
            # we reduced to a single bin. Put this bin in its own dimension
            for aname in allArrays:
                a = getattr(targetCont, aname)
                setattr(targetCont, aname,
                        a.reshape( (1,)+a.shape) )
        targetCont.drawStyle = self.drawStyle

        return targetCont
        



@BinContainer.toKnownClasses
class Histo2DContainer(Histo1DContainer):
    """Represents a set of 2D histograms arranged in a multi-dimension binned phase space"""

    # what members will be persistified
    #persistentAttr = ['x_'+a for a in AxisSpec.save_att]+['y_'+a for a in AxisSpec.save_att]
    persistentAttr = ['xaxis_s', 'yaxis_s'] # axis_s is a temporary member set during saving/loading
    
    persistPrefix = 'H2C'

    yaxis = None
    numInnerDim = 2
    def __init__(self, name, phspace, hspec=None, initArrays=True, ):
        """ phspace: the PhaseSpace representing the bins of the phase space in which the histos are kept.
            hspec:  in the form (x_nbins, (x_min, x_max), y_nbins, (y_min, y_max) ) or (...same as before..., "name:title" )"""
        if hspec is not None:
            if len(hspec)==2:
                # assume we are given 2 AxisSpec
                self.xaxis = hspec[0]
                self.yaxis = hspec[1]
                self.nbins = self.xaxis.nbins * self.yaxis.nbins
            else:
                # we are given a tuple 
                if len(hspec)==4 : hspec=tuple(hspec)+(f'{name}:xtitle:ytitle',)
                xnbins, xrange,ynbins, yrange, hname = hspec
                hname = hname.split(':')
                self.xaxis = AxisSpec( hname[0], nbins=xnbins, range=xrange,title=hname[1])
                self.yaxis = AxisSpec( hname[0], nbins=ynbins, range=yrange,title=hname[-1])
                self.nbins = xnbins*ynbins

        else:
            self.xaxis = AxisSpec('xaxis')
            self.yaxis = AxisSpec('yaxis')
        BinContainer.__init__(self,name, phspace,  initArrays,)
        if phspace: self.setup()
        self.persistNonArrays = []#'x_binDescriber','y_binDescriber']
                    

    def setup(self):
        BinContainer.setup(self)
        #if hasattr(self, 'bins'): self.nbins = self.bins # TEMPORARY to read back older version

    def initArrays(self):
        shape = [a.nbins for a in self.phspace.axis]+[self.xaxis.nbins+2, self.yaxis.nbins+2] # +2 : overflow and underflow nbin 
        self.array = np.zeros( shape , dtype='float32')
        self.arrayw2 = np.zeros( shape, dtype='float32' )
        self.entriesPerH = np.zeros( shape[:-1] ,dtype=int)
            

    def loadFromH5(self, fname, loadContent=True, encoding=None):
        # load all attributes
        BinContainer.loadFromH5(self, fname, loadContent, encoding)

        # get back xaxis  from self
        from pickle import loads
        self.xaxis=loads(self.xaxis_s.tobytes())
        self.yaxis=loads(self.yaxis_s.tobytes())
        del(self.xaxis_s)
        del(self.yaxis_s)
            
    def saveInH5(self, fname=None, mode='a', ):
        from pickle import dumps
        self.xaxis_s=np.bytes_(dumps(self.xaxis))
        self.yaxis_s=np.bytes_(dumps(self.yaxis))
        BinContainer.saveInH5(self, fname, mode)
        # cleanup ourselves
        del(self.xaxis_s)
        del(self.yaxis_s)
        
    def histNbins(self,):
        return (self.xaxis.nbins+2)*(self.yaxis.nbins+2)


    ## ********************************
    # Filling methods
    def fill(self, coords, values, weights=1.):
        """Fill histos in nbin given by coords with values.
           coords = array( (ndim, nvalues), dtype=float32)
           values = array( (nvalues,), dtype=float32) 
        """
        # allocate arrays for phase space indices        
        indices = np.zeros( (self.phspace.nDim()+2, values.shape[1])  , dtype =int )
        
        # find indices of coords falling in valid nbin :
        tupleI,validCoordI = self.phspace.findValidBins(coords,indices[:-2,:] , returnFlatI=True)
        # validCoordI is the indices of values corresponding to valid bins

        # prepare weights corresponding to valid bins 
        if isinstance(weights, np.ndarray):
            weights = weights[validCoordI]

        # finalize fill operations (this will compute histo indices and actually append values to histos)
        self.fillAtIndices( values,  validCoordI, tupleI,weights, weights*weights, indices[-2:] )
            

    def histBinIndex(self,values, out=None, flatInd=True):
        """Returns the indices of values in the 2D histogram of this Histo2DContainer
           values = array( shape=(2,Nvalues) )
        if given, out must have shape=(2,Nvalues)
        returns a array( shape=(Nvalues,), int) or array( shape=(2,Nvalues), int) (if flatInd=False)
        """
        hIndices = out
        if hIndices is None:
            n = values[0].size
            hIndices = np.zeros( 2*n, dtype=int).reshape( 2, n)

        self.xaxis.histobinIndex( values[0], hIndices[0])
        self.yaxis.histobinIndex( values[1], hIndices[1])
        if flatInd:
            fullIndices(hIndices[0], self.yaxis.nbins+2, hIndices[1] )
            return hIndices[1]
        else:
            return hIndices


    def histoAt(self, coords):
        hcontent = self.array[coords]
        hname=self.nameAt( coords )
        h = Histo2D(name=hname, xaxis=self.xaxis,yaxis=self.yaxis,
                   wcounts = hcontent,
                   w2counts = self.arrayw2[coords],
                   title = self.label,
                   )
        return h




    def __getitem__(self, args):
        """Support for slicing with the operator []. Let bh is a Histo1DContainer, then 
         (see Histo1DContainer.__getitem__ for doc)
      
         UNTESTED yet ...
        IMPORTANT : this uses numpy slices, so the array of the returned Histo1DContainer is shared with its parent whenever possible.
                    when summing the returned Histo1DContainer always contains different arrays. 
        """

        print(" WARNING Histo2DContainer slicing untested yet !",args)        
        # retrieve the sliced histos and which bin/dimensions to sum
        phspace, finalslices, dimtobsummed = self.phspace.sliceWithSum(args) #phspaceFromSliceSum(self.phspace, args)

        if self.array is None:
            self.loadContent()
        
        # the new Histo1DContainer
        bh = Histo2DContainer( self.name+'__',phspace, hspec=(self.xaxis,self.yaxis), initArrays=False)

        # assign bh's arrays
        self._slicearrays(bh, finalslices, dimtobsummed)

        if phspace.nbins==1:
            return  bh.histoAt( (0,))

        return bh

