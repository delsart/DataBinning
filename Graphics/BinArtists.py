""" 
This module implements a set of classes which encapsulate the draw operations for 
  - one specific draw task (ex: a line, a histo, a text legnd)
  - for one bin described by 1 coordinates (integer coordinates like (i,j,k))
  - given a BinContainer



They are intended to be used by GraphicSession methods which will call them on a user specificied combination of bins and BinContainer.


"""
import matplotlib
from copy import copy

class BinArtist:
    """A base class to help to automatize  the drawing of BinContainer
    """
    def __init__(self):
        self.axes = []
        self.bps = None

    def setup(self, bps, contList, figs):
        self.bps = bps
        self.contList = contList # list of BinContainer on which to operate
        for i,bh in enumerate(contList):
            bh.draw_i = i # just set the drawing order of this BinContainer so BinArtist can use it.
        try:
            figs = iter(figs)
        except:
            figs=[figs]
        for fig in figs:
            self.add_figure(fig)
        
    def add_figure(self, fig):
        self.axes += fig.axes
        
    def draw(self, cont, coord, ax, ):
        """Implement the draw operation for the  content of BinContainer 'cont' at coordinates 'coord' and onto axes 'ax'
        This functions will be called for each cont to be drawn on a same ax .
        """
        return []

    def drawAtBin(self,  coord, ax, plotOpts={}):
        """Implement draw operations to be done only once per coordinates 'coord' on axes 'ax' 
        ex: draw text describing the bin at 'coord'
        """
        return []
    
class BAHisto1D(BinArtist):
    """Draws histograms in each bin (assuming it is used on HistoContainer)"""
    def setup(self, bps, contList, fig):
        self.label = getattr(contList[0], 'hdesc' ,'')
        
    def drawAtBin(self,  coord, ax, plotOpts={}):
        ax.set_xlabel(self.label)
        
    def draw(self, cont, coord, ax, ):
        self.curve=cont.drawAt( coord, same=True,  ax=ax, **cont.drawStyle)
        return self.curve

class BAHisto2D(BinArtist):
    """Draws histograms in each bin (assuming it is used on HistoContainer)"""
    def setup(self, bps, contList, fig):
        self.label = getattr(contList[0], 'hdesc' ,'')
        
    def drawAtBin(self,  coord, ax, plotOpts={}):
        ax.set_xlabel(self.label)
        
    def draw(self, cont, coord, ax, ):
        self.curve=cont.drawAt( coord,   ax=ax, **cont.drawStyle)
        return self.curve
    

class BABinDesc(BinArtist):
    fontsize_def = "xx-large"
    def __init__(self, x0=0.63,y0=0.75,dy=0.06, fontsize=None,addBinN=False, addText=[]):
        super().__init__()
        self.x0=x0
        self.y0=y0
        self.fontsize=fontsize or self.fontsize_def
        self.addBinN  = addBinN
        self.addText = addText
        
    def add_figure(self,  fig):
        super().add_figure(fig)
                
    def drawAtBin(self,  coord, ax, plotOpts={}):
        descs = self.bps.describeBin( coord )
        if self.addBinN: descs.append( str(coord) )
        fullStr='\n'.join(descs+self.addText)
        ax.text(self.x0,self.y0,fullStr,transform=ax.transAxes,fontsize=self.fontsize)
        return []

class BALine(BinArtist):
    """Base class of drawing simple lines """
    def __init__(self,  ):
        super().__init__()
        self.lines = []
        self.tmpl = np.array( [[0,1],[0,1]] )
        
    def add_figure(self,  fig):
        super().add_figure(fig)


class BAGraphXY(BinArtist):
    """Draws the 2D graphs in each bin (assuming it is used on CurveContainer objects)"""
    def draw(self, cont, coord, ax, ):        
        self.curve=cont.drawAt( coord, ax=ax, **cont.drawStyle )
        return self.curve
        
class BARespLine(BALine):
    """Draws a vertical line at x = cont.respname[coord] """
    def __init__(self, respname='resp' ):
        super().__init__()
        self.respname = respname

    def setup(self, bps, contList, fig):
        super().setup(bps,contList,fig)
        self.label = getattr(contList[0], 'hdesc' ,'')
        #self.x_array =[ getattr(cont,self.respname) for cont in contList ]
        
    def draw(self, cont, coord, ax, plotOpts={}):
        #i = ax.figure.axes.index(ax)
        #x = self.x_array[cont.draw_i][coord]
        x=getattr(cont, self.respname)[coord]
        #l = self.lines[i][cont.draw_i]
        #print( l , id(l)  , x )
        #l.set_data([x,x],[0,ax.get_ylim()[1]] )
        ax.plot( [x,x],[0, ax.get_ylim()[1] ] , **cont.drawStyle)
        #ax.add_line(l)
        return []


class BAFixedLine(BinArtist):
    """Draw fixed horizontal or vertical line at position p0.
    And also 2 lines at pos p0+/-delta if delta>0.
    """
    def __init__(self, p0=1, delta=0, lw=1.,ls='-', ls_d='--',lw_d=0.5, color='grey', color_d='grey' ):
        super().__init__()
        self.p0 = p0
        self.delta = delta
        self.style = dict(ls=ls,lw=lw,color=color)
        self.style_d = dict(ls=ls_d,lw=lw_d,color=color_d)
        self.setupLines()

    def setupLines(self):
        L = matplotlib.lines.Line2D
        x, y = self.xpos[0], self.ypos[0]
        self.lines=[L(x,y,**self.style ) ,]
        if self.delta :
            self.lines+= [
                L(self.xpos[1],self.ypos[1],**self.style_d),
                L(self.xpos[2],self.ypos[2],**self.style_d),
            ]
        
    def drawOnAx(self,ax):
        self.drawAtBin(None, ax)
        
class BAFixLineH(BAFixedLine):
    def __init__(self, y0=1, dy=0.01, **style ):
        self.ypos = ( [y0,y0], [y0-dy,y0-dy], [y0+dy,y0+dy] )
        self.xpos = ( [0,1],) *3        
        super().__init__(p0=y0,delta=dy, **style)
            
                
    def drawAtBin(self,  coord, ax, plotOpts={}):
        xlim = ax.get_xlim()
        for l in [copy(_l) for _l in self.lines]:
            l.set_xdata( xlim )
            ax.add_line(l)
        return []

class BAFixLineV(BAFixedLine):
    def __init__(self, x0=1, dx=0, **style ):
        self.xpos = ( [x0,x0], [x0-dx,x0-dx], [x0+dx,x0+dx] )
        self.ypos = ( [0,1],) *3        
        super().__init__(p0=x0,delta=dx, **style)
                        
    def drawAtBin(self,  coord, ax, plotOpts={}):
        ylim = ax.get_ylim()
        for l in [copy(_l) for _l in self.lines]:
            l.set_ydata( ylim )
            ax.add_line(l)
        return []
