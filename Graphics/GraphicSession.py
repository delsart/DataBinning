import os
import matplotlib
import matplotlib.pyplot as plt

from .BinArtists import *
from ..BinContainer import BinContainer
from .Page import buildPage, hasMplCairo
from .HistoGraphics import _



class GraphicSession:
    """ Helper class to deal with multiple bincontainer : 
      - read them from file
      - methods to produce multiple plots containing multiple lines/graphs/histos...
    """

    inputDir = ''
    plotDir = ''


    colorStock = {'black', 'red', 'blue', 'green', 'purple', 'peru', 'gray', 'cyan', 'maroon'}
    
    # will be a list containing all bincontainer in the session
    allCont = None
    
    def __init__(self, **args):
        self.allCont = []
        for k,v in args.items():
            setattr(self,k,v)


    def outFile(self, outname):
        if self.plotDir: os.makedirs(self.plotDir, exist_ok=True)
        return os.path.join(self.plotDir, outname)



    def readContFromFile(self,fname, loadContent=False, encoding=None, exclude='', tag='', **drawStyle):
        """Reads all bincontainer from file fname and sets then as members of self.
        Also assign bincontainer graphic style as given by **drawStyle options. 
          - if drawStyle is void a color is picked from self.colorStock
          - if drawStyle contains styleFunc : this function is used to generate the dict of styles.
        ex : 
         graphSess.readContFromFile(fname, tag='myTag', color='blue', ls='--')
         graphSess.readContFromFile(fname, styleFunc = lambda ba : dict(color='r', label=ba.name+'AA' ) )
        """
        _, allCont = BinContainer.readContFromFile(fname,loadContent=loadContent,store=self,exclude=exclude, tag=tag)
        self.allCont += allCont

        styleFunc = drawStyle.pop('styleFunc', None)
        
        if styleFunc is None:
            styleFunc = lambda ba:drawStyle

        for ba in allCont:
            ba.tag = tag
            ba.drawStyle = styleFunc(ba)
            self.assertStyleHasColor(ba.drawStyle)
            ba.drawStyle.setdefault('label', tag)

    def assertStyleHasColor(self, drawStyle):
        c = drawStyle.get('color',None)
        if c is None:
            drawStyle['color'] = self.colorStock.pop()
        else:
            self.colorStock.discard(c)
        
        
    
    def drawBins(self, bhList, artists, outname='', coords=None,  nplots=4, page= None,   cslice=None, **axOpts):
        """Generic function to draw content of several bincontainer in several plots, each plot corresponding to 1 coordinate of the phase space. 
        Typically, this is used to dump many plots in a pdf file or as a series of images.
        See drawManyGraphs and drawManyHistos for concrete examples. """
        #nplots=4 if coords is None or len(coords)>1 else 1
        if cslice is not None:
            bhList = [bh[cslice] for bh in bhList ]
        phspace= bhList[0].phspace

        # guess the suffix if needed :
        suffix = axOpts.pop('suffix', outname.split('.')[-1] if '.' in outname else '')

        interactive = outname==''
        bckend = setupMPL( suffix=='pdf',  interactive) # attempt to get faster pdf generation
        bckend = matplotlib.get_backend()

        # setup the Page which contain the matplotlib figure&axes
        if page is None:
            page = buildPage(suffix, nplots, self.outFile(outname) ,figsize=axOpts.pop('figsize',None) )
        else:
            page.setNplots( nplots )
            

        if not isinstance(nplots, int): nplots = nplots[0]*nplots[1]

        a0 = page.figure.axes[0]

        nBin0 = phspace.a0.nbins
        if coords is None : coords = phspace.iter()
        elif isinstance(coords, slice):
            coords=phspace.indices[coords]

        # prepare the artists ------------
        artists += axOpts.pop('otherArtists',[])        
        for a in artists:
            a.setup(phspace, bhList, page.allFigures())

        # options to be passed to matplotlib axis.set( )
        # We want to support a bit more than what axis.set( ) supports : we put them in specialOptFunc
        specialOptFunc , stdOpts =[], {}
        for optName, optV in axOpts.items():
            if optName=="bottom": specialOptFunc.append( lambda a : a.set_ylim(bottom=optV) )
            elif optName=="top": specialOptFunc.append( lambda a : a.set_ylim(top=optV) )
            elif optName=="left": specialOptFunc.append( lambda a : a.set_xlim(left=optV) )
            elif optName=="right": specialOptFunc.append( lambda a : a.set_xlim(right=optV) )
            elif optName=="grid": specialOptFunc.append( lambda a : a.grid(**optV) )
            else:
                stdOpts[optName] = optV

        count = 0
        for c in coords:
            c = tuple(c)
            ax = page.figure.gca()
            ax.cla()

            print('draw at ', c)
            for bh in bhList:
                for a in artists:
                    a.draw( bh,c, ax)
            for a in artists:
                a.drawAtBin( c, ax )

            for f in specialOptFunc:
                f(ax)
            ax.set( **stdOpts)
            
            if (count%nplots)==0: ax.legend()

            if c[0]==(nBin0-1):
                # restart from a blank page after we plotted a full pt slice
                if not interactive: page.forceSave()
                count=0
            else:
                page.nextAx()
                count +=1 
            #if count==100:break
        if suffix : page.close()
        self.page = page
        matplotlib.use(bckend)
        plt.ion()
        return page


    def drawManyHistos(self, bhList,  coords=None,  outname='', nplots=4,   **plotOpts):
        """Draw multiple plots of several histos from a list of HistoContainer.
         * Loop on coordinates defined by 'coords' (an array of bin coordinates as in phspace.indices[ something] )
         * For each coordinate  draw 1 plot containing the histos taken from 'bhList' at that coordinate.
         * if saving on file (when outname ends with '.pdf', '.png', '.svg') several pages or images maybe saved, with 'nplots' per page/image.

        ex : drawManyHistos( [mR_in_EMEta_uncal, mR_in_EMEta_dnn], outname='massResp.pdf', )
            -> draw ALL the histos in the 2 input HistoContainer, with 4 plots per page in the PDF file 'massResp.pdf'

        ex : drawManyHistos( [mR_in_EMEta_uncal, mR_in_EMEta_dnn], coords=np.s_[4,:2,12], nplots=2)
           -> Draw 2 plots 1 for coordinate (4,0,12), 1 for (4,1,12) containing each the corresponding histos from the input HistoContainer. No output file saved, only shown on an interactive window.


        List of honored options :
         - binDesc : a dict, passed to BABinDesc(). ex: addBinN=True,y0=0.8,fontsize='large'
         - otherArtists : a list of configured BinArtists
         - figsize : a tuple, the size of the matplotlib fig in inches (!)
         - suffix : a str

         - all other matplotlib.Axes attributes (xlabel, ylabel, etc...)
        
        """
        artists = [ BAHisto1D(), BABinDesc(**plotOpts.pop('binDesc',{})), ]
        return self.drawBins(bhList, artists, outname, coords,  nplots,  **plotOpts)

    def drawManyHistos2D(self, bh,  coords=None,  outname='', nplots=4, **plotOpts):
        binDesc = plotOpts.setdefault('binDesc',{})
        binDesc.update(fontsize='large')
        artists = [ BAHisto2D(), BABinDesc(**binDesc), ]
        return self.drawBins([bh], artists, outname, coords,  nplots, **plotOpts)
    

    def drawManyGraphs(self, bhList, coords=None, outname=None, nplots=4, **plotOpts):
        """Same as drawManyHistos but for Lines/Graphs """
        artists = [  BAGraphXY(), BABinDesc(**plotOpts.pop('binDesc',{})), ]
        plotOpts.setdefault('xlabel', bhList[0].xlabel)
        plotOpts.setdefault('ylabel', bhList[0].ylabel)
        return self.drawBins(bhList, artists, outname, coords, nplots, **plotOpts)


def asGraphicSessionMethod(func):
    setattr(GraphicSession, func.__name__, func)

def setupMPL(usePdf, interactive):
    bckend = matplotlib.get_backend()
    if usePdf:
        if hasMplCairo:
            matplotlib.use("module://mplcairo.base")
            matplotlib.rcParams['agg.path.chunksize'] = 1000
        plt.ioff()        
    elif not interactive:
        matplotlib.use("cairo")
        plt.ioff()
    else:
        matplotlib.use("QtAgg")
        plt.ion()
    return bckend
    
