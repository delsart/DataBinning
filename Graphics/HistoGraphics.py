import matplotlib
import matplotlib.pyplot as plt
from ..Histogram import Histo1D,Histo2D
from ..HistoContainer import Histo1DContainer,Histo2DContainer
from ..CurveContainer import CurveContainer
import numpy as np
_=None

def addToClass(cls):
    """Decorator to add the decorated function to the class 'cls' """
    def funcAdder(func):
        setattr(cls,func.__name__,func)
        return func
    return funcAdder


def _clearaxis(ax):
    if hasattr(ax, 'colorbar'):
        ax.colorbar.remove()
        del(ax.colorbar)
    ax.clear()
    ax.set_aspect('auto') # needed after calling matshow
def _drawOpts(h,useopts):
    opts = h.drawStyle or {}
    opts.update(**useopts)
    return opts
## **********************************************************
## Histo 1D 
@addToClass(Histo1D)
def draw(self, ax=None, same=False,  err=False, ylim=None,xlim=None, yscale='linear', title=None,**opts):
    if ax is None: ax=plt.gca()
    if not same: _clearaxis(ax)
    opts.setdefault('label', self.title)
    opts = _drawOpts(self, opts)
    
    if err:
        opts['ds']='steps-post'
        c = self.xaxis.edges[:-1]        
        rr= ax.errorbar(c, self.wcounts[1:-1], np.sqrt(self.w2counts[1:-1]), **opts )
    else:
        opts.pop('ds',None)
        rr = ax.stairs( self.wcounts[1:-1], self.xaxis.edges, **opts )

    if not same:
        ax.set_xlabel(self.xaxis.title)
        ax.set_title(title or self.title)
        ax.set_yscale(yscale)
        if ylim: ax.set_ylim(ylim)
        if xlim: ax.set_xlim(xlim)
    return rr


## **********************************************************
## Histo 2D 
@addToClass(Histo2D)
def draw(self,ax=None, zmax=None,norm=None,title=None,**opts):
    if ax is None: ax=plt.gca()
    _clearaxis(ax)
    m=ax.pcolormesh(self.xaxis.edges,self.yaxis.edges,self.wcounts[1:-1,1:-1].T, cmap=opts.pop('cmap','Reds'),vmax=zmax,norm=norm)
    # ax.set_xlabel(self.xaxis.title)
    # ax.set_ylabel(self.yaxis.title)
    ax.set_title(title or self.title)
    ax.set_ylabel(self.yaxis.title)
    ax.set_xlabel(self.xaxis.title)
    opts.setdefault('label', self.title)
    opts = _drawOpts(self, opts)    
    ax.set(**opts)
    cb=plt.colorbar(m, ax=ax)
    ax.colorbar = cb
    m.axes = ax # needed so it colorbar can remove itself from the axes



def splitEqually(n,k):
    r , d = n % k, n//k
    nd = np.full(k+1, d)
    nd[0] = 0
    for i in range(r):
        nd[i+1]+=1
    return nd.cumsum()[:-1]
    
@addToClass(Histo2D)
def _drawProjections(self, proji,  bins=5,  ax=None, title=None,**opts):
    """ Draw several 1D histograms projected onto axis proji.
      bins : how to create the 1D histograms:
        * if int, create n=bins histograms, each corrsponding to equally splited intervals along the other axis (and cumulating these intervals)          
        * if array in the form [x0, x1, ...,xN] then create N-1 histos cummulating bins as in [bin(x0),bin(x1)], [bin(x1),bin(x2)], ... 
    """
    if ax is None: ax=plt.gca()
    _clearaxis(ax)
    projAx = self.axes[proji]

    if isinstance(bins, int):
        binsi = np.concatenate([ splitEqually(projAx.nbins, bins),[projAx.nbins] ])+1
    else:
        # assumes
        bins = np.array(bins)
        binsi = projAx.histobinIndex(bins)


    slices = [slice(None), slice(None)]
    projVar = projAx.name
    
    print('binsi=',binsi)
    hL=[]
    for i, lowb in enumerate(binsi[:-1]):
        higb = binsi[i+1]
        print('aaaa', lowb, higb)
        #h = self[lowb:higb:sum,:]
        slices[proji] = slice(lowb,higb,sum)
        h = self[*slices]
        c=projAx.binCenter(i)
        h.draw(label=f'{projVar}={c}',same=i>0)
        hL.append(h)
    plt.gca().legend()
    return hL

@addToClass(Histo2D)
def drawYprojections(self, bins=5,  ax=None, title=None,**opts):
    return self._drawProjections(0,bins,ax,title, **opts)
@addToClass(Histo2D)
def drawXprojections(self, bins=5,  ax=None, title=None,**opts):
    return self._drawProjections(1,bins,ax,title, **opts)


@addToClass(Histo2D)
def drawMatrix(self,ax=None, zmax=None,norm=None,title=None,**opts):
    if ax is None: ax=plt.gca()
    _clearaxis(ax)

    if norm=='log':
        from matplotlib.colors import LogNorm
        norm=LogNorm()

    data=self.wcounts.T
    ax.matshow(data,  norm=norm,cmap=opts.pop('cmap','Reds'),vmax=zmax)

    for (i, j), z in np.ndenumerate(data):
        ax.text(j, i, '{:0.1f}'.format(z), ha='center', va='center')


## **********************************************************
## Histo1DContainer 
@addToClass(Histo1DContainer)
def drawAt(self, coords, ax=None, **opts):
    if ax is None: ax=plt.gca()
    if self.array is None:self.loadContent()
    h = self.histoAt(coords)
    return h.draw(ax=ax,**opts)


## **********************************************************
## Histo2DContainer 
@addToClass(Histo2DContainer)
def drawAt(self, coords, ax=None, **opts):
    if self.array is None:self.loadContent()
    h = self.histoAt(coords)
    return h.draw(ax=ax,**opts)


## **********************************************************
## CurveContainer 
@addToClass(CurveContainer)
def drawAt(self, coords, ax=None, err=True, **opts):
    from matplotlib import pyplot as plt
    if ax is None: ax=plt.gca()
    if self.array_x is None:self.loadContent()
    n=self.npoints[coords]
    x = self.array_x[coords][:n]
    y = self.array_y[coords][:n]        
    if self.withErr and err :            
        y_err = self.array_yerr[coords][:n]
        return ax.errorbar(x,y,y_err,**opts)
    else:
        return ax.plot(x,y,**opts)
