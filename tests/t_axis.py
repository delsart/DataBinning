import unittest
import numpy as np
from DataBinning.Axis import AxisSpec

class TestAxis(unittest.TestCase):
    def setUp(self):
        self.genaxis = AxisSpec("gen", title="xx",edges=np.array([0.,1,2,3,10, 12.5]) ,
                                binNames = ["b0", "b1", "b2" , "b3", "b4"])
        self.regaxis = AxisSpec("reg", title="xx", nbins=10, range=(0,1.), )
        self.intaxis = AxisSpec("int", title="xx", nbins=10, )

        self.nameaxis = AxisSpec("nam", binNames=["d0", "d1", "d2"] )
    
    def test_base_gen(self):
        self.assertEqual(self.genaxis.edges[0], 0., "genaxis incorrect low edge")
        self.assertEqual(self.genaxis.edges[-1], 12.5, "genaxis incorrect high edge")
        self.assertEqual(self.genaxis.nbins, 5, "genaxis incorrect nbins")
        self.assertEqual(self.genaxis.range, (0.,12.5) , "genaxis incorrect nbins")
        self.assertEqual(self.genaxis.binWidth(0), 1. , "genaxis incorrect binwidth 0")
        self.assertEqual(self.genaxis.binWidth(3), 7. , "genaxis incorrect binwidth 0")
        self.assertEqual(self.genaxis.binWidth(1,histobin=True), 1. , "genaxis incorrect histo binwidth 0")
        self.assertEqual(self.genaxis.binWidth(4,histobin=True), 7. , "genaxis incorrect  histo binwidth 0")
        self.assertEqual(self.genaxis["b1"], (1.,2.) , "genaxis wrong bin by name b1")

        self.assertEqual(self.genaxis.isRegular, False, "genaxis is regular")
        self.assertEqual(self.genaxis.isIntBin, False, "genaxis is int bin")

        np.testing.assert_array_equal(self.nameaxis.binCenter(), np.array([0.5, 1.5, 2.5],dtype=np.float32), "binCenter wrong")
        
    def test_base_int(self):
        
        self.assertEqual(self.intaxis.edges[0], 0., "intaxis incorrect low edge")
        self.assertEqual(self.intaxis.edges[-1], 10., "intaxis incorrect high edge")
        self.assertEqual(self.intaxis.nbins, 10, "intaxis incorrect nbins")
        self.assertEqual(self.intaxis.range, (0,10) , "intaxis incorrect nbins")
        self.assertEqual(self.intaxis[1], 1 , "intaxis wrong bin by name b1")
        self.assertEqual(self.intaxis.isRegular, True, "intaxis not regular")
        self.assertEqual(self.intaxis.isIntBin, True, "intaxis not int bin")
        

    def test_base_reg(self):
        self.assertEqual(self.regaxis.edges[0], 0., "regaxis incorrect low edge")
        self.assertEqual(self.regaxis.edges[-1], 1., "regaxis incorrect high edge")
        self.assertEqual(self.regaxis.nbins, 10, "regaxis incorrect nbins")
        self.assertEqual(self.regaxis.range, (0.,1.) , "regaxis incorrect nbins")

        self.assertEqual(self.regaxis.isRegular, True, "regaxis not regular")

        self.assertEqual(self.regaxis.isIntBin, False, "regaxis is int bin")



    def test_io(self):
        d = {}
        self.genaxis.saveTo(d)
        self.genaxis2 = AxisSpec("gen")
        self.genaxis2.loadFrom(d)

        self.genaxis = self.genaxis2
        self.test_base_gen()
        
        
        
    def test_describe(self):
        pass

    def test_findBins(self):
        toBin = np.array([-3, 0, 0.02, 0.5, 0.66, 1., 1.3] )
        np.testing.assert_array_equal( self.regaxis.histobinIndex( toBin ), np.array([ 0,  1,  1,  6,  7, 11, 11]), "regaxis.histobinIndex failed" )

        indices , validity = self.regaxis.binIndexAndValidity( toBin ) 
        np.testing.assert_array_equal( indices[1:-2] , np.array([0, 0, 5, 6]) , "regaxis.binIndexAndValidity wrong valid index" )
        np.testing.assert_array_equal( validity , np.array([False,  True,  True,  True,  True, False, False]) , "regaxis.binIndexAndValidity wrong validity array" )

        toBin = np.array([-3, 0, 0.02, 1.2, 3.5, 12., 145] )
        np.testing.assert_array_equal( self.genaxis.histobinIndex( toBin ), np.array([0, 1, 1, 2, 4, 5, 6 ]), "genaxis.histobinIndex failed" )

        indices , validity = self.genaxis.binIndexAndValidity( toBin ) 
        np.testing.assert_array_equal( indices[1:-1] , np.array([0,  0,  1,  3,  4,]) , "genaxis.binIndexAndValidity wrong valid index" )
        np.testing.assert_array_equal( validity , np.array([False,  True,  True,  True,  True,  True, False]) , "genaxis.binIndexAndValidity wrong validity array" )
        
        
        

# for interactive tests :
t = TestAxis()
t.setUp()
    
unittest.main()

