import unittest
import numpy as np
from DataBinning.PhaseSpace import PhaseSpace, rm
from DataBinning.Axis import AxisSpec
from DataBinning.NumberContainer import ScalarContainer, StatContainer
from DataBinning.BinContainer import BinContainer




bin3_1_x=5.2
bin3_1_y=13.0

coords = [ np.array( [ 0.5,   3., 3.2,   3.3,bin3_1_x,bin3_1_x, 10,    -1, 17, 1.5]),
           np.array( [ 1., 12.2 , 10.2, 11.3,bin3_1_y,bin3_1_y, 15,    1,  -1, 20.,]),
          ]
svalues = np.array( [ 5. ,   10, 20, 100., 0.1 , 0.01,     3.1 ] )

xvalues = np.array( [ 1.1 ,   1.1, 2.1, 2.1, -0.2, 10.2,   3.1, -15,-15,-15] )
yvalues = np.array( [ 0. ,   -0.1, .1, .1,  -1.2, 1.3,     3.1, -15,-15,-15] )


class TestNumCont(unittest.TestCase):
    def setUp(self):
        self.ps1 = PhaseSpace("PS",
                              AxisSpec("xax", title="xx",edges=np.array([0,1,2,3,10, 12.5]) ),
                              AxisSpec("yax", title="yy",edges=np.array([0,10, 15, 20]) ),
                              )


        self.sc = ScalarContainer("sc",self.ps1, initArrays=True)


        self.ps2 = PhaseSpace("PS3d",
                              AxisSpec("xax", title="xx",edges=np.array([0,1,2,3,10, ]) ),
                              AxisSpec("yax", title="yy",edges=np.array([0,10, 15, 20]) ),
                              AxisSpec("zax", title="zz",edges=np.array([100,1000, 2000]) ),
                              )


        self.sc = ScalarContainer("sc",self.ps1, initArrays=True)
        
        self.sc2 = ScalarContainer('sc2', self.ps2, initArrays=True)

        self.sc2.values[0,0,0]=1.
        self.sc2.values[0,0,1]=10.

        self.sc2.values[2,0,0]=-2.

        self.sc2.values[1,1,0]=50.
        self.sc2.values[1,1,1]=100.

        self.st = StatContainer('st', self.ps1, initArrays=True)
        
    def test_fillSc(self):
        tupleI, validCoordI = self.sc.phspace.findValidBins( (xvalues,yvalues), returnFlatI=True)

        self.sc.values.ravel()[tupleI]= svalues[validCoordI]

    def test_psslice(self):
        ps2= self.ps2
        self.assertEqual( ps2.sliceWithSum(np.s_[:3,1:,:])[0].shape() ,  (3,2,2) , "wrong sliced shape :3,1:,: " )
        self.assertEqual( ps2.sliceWithSum(np.s_[:3,1:,::sum])[0].shape() ,  (3,2) , "wrong sliced shape :3,1:,::sum " )
        self.assertEqual( ps2.sliceWithSum(np.s_[:3,1:,1::sum])[0].shape() ,  (3,2) , "wrong sliced shape :3,1:,1::sum " )
        self.assertEqual( ps2.sliceWithSum(np.s_[:,sum,:])[0].shape() ,  (4,2) , "wrong sliced shape :,sum,: " )
        self.assertEqual( ps2.sliceWithSum(np.s_[:,2,:])[0].shape() ,  (4,2) , "wrong sliced shape :,2,: " )

    def test_slice(self):
        np.testing.assert_array_equal(self.sc2[sum,1:,:].values, np.array([[ 50., 100.],[  0.,   0.]],) , f"wrong values for [sum,1:,:]")
        np.testing.assert_array_equal(self.sc2[sum,:,:].values, np.array([[ -1.,  10.],[ 50., 100.],[  0.,   0.]],) , f"wrong values for [sum,1:,:]")

        np.testing.assert_array_equal(self.sc2[:3,:2,sum].values, np.array([[ 11.,   0.], [  0., 150.], [ -2.,   0.]],) , f"wrong values for [sum,1:,:]")


    def test_stat(self):
        self.st.accumulate( (xvalues,yvalues), svalues )

        np.testing.assert_array_equal(self.st.sumw , np.array([[0., 0., 0.], [1., 0., 0.],[2., 0., 0.],[1., 0., 0.],[1., 0., 0.]],), "stat : wrong sumw" )
        
t = TestNumCont()
t.setUp()

unittest.main()
        
