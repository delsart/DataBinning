import unittest
import numpy as np
from DataBinning.PhaseSpace import PhaseSpace, rm
from DataBinning.Axis import AxisSpec
from DataBinning.HistoContainer import Histo1DContainer,Histo2DContainer
from DataBinning.BinContainer import BinContainer




def npa(l):
    return np.array(l,dtype=np.float32)

xrange= (0,10)
yrange= (-1,1)

bin3_1_x=5.2
bin3_1_y=13.0

coords = [ np.array( [ 0.5,   3., 3.2,   3.3,bin3_1_x,bin3_1_x, 10,    -1, 17, 1.5]),
           np.array( [ 1., 12.2 , 10.2, 11.3,bin3_1_y,bin3_1_y, 15,    1,  -1, 20.,]),
          ]
svalues = np.array( [ 5. ,   10, 20, 100., 0.1 , 0.01,     3.1 ] )

xvalues = np.array( [ 1.1 ,   1.1, 2.1, 2.1, -0.2, 10.2,   3.1, -15,-15,-15] )
yvalues = np.array( [ 0. ,   -0.1, .1, .1,  -1.2, 1.3,     3.1, -15,-15,-15] )

weights =np.array( [ 1.,     1. , 0.5, 1.2, 1.2  , 0.5,    1  , -15,-15,-15   ])

def buildH1(ps,  values, weights):
    bh = Histo1DContainer( "hps1", ps, (10,xrange))
    bhflat = Histo1DContainer( "hps1Flat", ps, (10,xrange))

    bh.fill( coords, values, weights )

    binIndices = ps.emptyCoordinates(values.size)
    flatInd, validCoordI = ps.findValidBins(coords, binIndices  , returnFlatI=True)
    bhflat.fillAtIndices( values, validCoordI, flatInd, validW=weights)

    return bh, bhflat


def buildH2(ps, values, weights):
    bh = Histo2DContainer( "hps2", ps, (10,xrange, 8,yrange))
    bh.fill( coords, values, weights )

    return bh



class TestHistoCont(unittest.TestCase):
    def setUp(self):
        self.ps1 = PhaseSpace("PS",
                              AxisSpec("xax", title="xx",edges=np.array([0,1,2,3,10, 12.5]) ),
                              AxisSpec("yax", title="yy",edges=np.array([0,10, 15, 20]) ),
                              )

                

    def test_fill(self):
        
        bh, bhflat = buildH1(self.ps1,xvalues, 1.)
        content_3_1 = np.array([1., 0., 1., 2., 0., 0., 0., 0., 0., 0., 0., 1.], )

        np.testing.assert_array_equal(bh.histoAt( (3,1) ).wcounts , content_3_1, f"wrong content at (3,1)")
        np.testing.assert_array_equal(bh.array, bhflat.array, f"bh and bhflat differs")
        

        
        bh, bhflat = buildH1(self.ps1, xvalues, weights)
        content_3_1 = npa([1.2, 0. , 1. , 1.7, 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0.5])
        content_3_1_w2 = npa([1.44, 0.  , 1.  , 1.69, 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  ,0.25])

        np.testing.assert_array_equal(bh.histoAt( (3,1) ).wcounts , content_3_1, f"wrong w at (3,1)")
        np.testing.assert_array_equal(bh.histoAt( (3,1) ).w2counts , content_3_1_w2, f"wrong w2 at (3,1)")
        np.testing.assert_array_equal(bh.array, bhflat.array, f"weighted bh and bhflat differs")

        self.hc = bh
        

    def test_fill2(self):
        bh2 = buildH2(self.ps1, np.stack( [xvalues, yvalues]),1.)

        zs=[0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]
        content_3_1 = np.array([[1., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                                zs, [0., 0., 0., 0., 1., 0., 0., 0., 0., 0.], [0., 0., 0., 0., 0., 2., 0., 0., 0., 0.], zs,zs, zs,zs,zs,zs,zs,[0., 0., 0., 0., 0., 0., 0., 0., 0., 1.]], dtype=np.float32)

        np.testing.assert_array_equal(bh2.histoAt( (3,1) ).wcounts, content_3_1, f"wrong histo2D content at (3,1)")

        bh2w = buildH2(self.ps1, np.stack( [xvalues, yvalues]),weights)
        content_3_1_w =np.array([[1.2, 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0. ],
                                 zs,[0. , 0. , 0. , 0. , 1. , 0. , 0. , 0. , 0. , 0. ], [0. , 0. , 0. , 0. , 0. , 1.7, 0. , 0. , 0. , 0. ],
                                 zs,zs,zs,zs,zs,zs,zs,[0. , 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0.5]], dtype=np.float32)

        np.testing.assert_array_equal(bh2w.histoAt( (3,1) ).wcounts, content_3_1_w, f"wrong histo2D weighted content at (3,1)")
        
        

    def test_io1D(self):
        bh, bhflat = buildH1(self.ps1,xvalues, weights)

        
        bh.saveInH5('bla.h5')
        
        bh2 = Histo1DContainer( "hps1", None, initArrays=False)

        bh2.loadFromH5('bla.h5')
        content_3_1 = npa([1.2, 0. , 1. , 1.7, 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0.5])
        content_3_1_w2 = npa([1.44, 0.  , 1.  , 1.69, 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  ,0.25])

        np.testing.assert_array_equal(bh2.histoAt( (3,1) ).wcounts , content_3_1, f"read back : wrong w at (3,1)")
        np.testing.assert_array_equal(bh2.histoAt( (3,1) ).w2counts , content_3_1_w2, f"read back : wrong w2 at (3,1)")
        np.testing.assert_array_equal(bh2.array, bhflat.array, f"read back : weighted bh and bhflat differs")
        
        self.assertEqual(bh.xaxis.describeBin( 3 ) , bh2.xaxis.describeBin( 3 ) , "read back : describeBin differs")

    def test_io2D(self):
        bh2 = buildH2(self.ps1, np.stack( [xvalues, yvalues]),weights)
        
        bh2.saveInH5('bla.h5')
        
        bh2r = Histo2DContainer( "hps2", None, initArrays=False)

        bh2r.loadFromH5('bla.h5')
        zs=[0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]
        content_3_1_w =np.array([[1.2, 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0. ],
                                 zs,[0. , 0. , 0. , 0. , 1. , 0. , 0. , 0. , 0. , 0. ], [0. , 0. , 0. , 0. , 0. , 1.7, 0. , 0. , 0. , 0. ],
                                 zs,zs,zs,zs,zs,zs,zs,[0. , 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0.5]], dtype=np.float32)

        np.testing.assert_array_equal(bh2r.histoAt( (3,1) ).wcounts, content_3_1_w, f"wrong histo2D weighted content at (3,1)")

        self.assertEqual(bh2r.xaxis.describeBin( 3 ) , bh2r.xaxis.describeBin( 3 ) , "read back : describeBin differs")
        self.assertEqual(bh2r.yaxis.describeBin( 3 ) , bh2r.yaxis.describeBin( 3 ) , "read back : describeBin differs")


    def test_readall(self):
        cstore,_=BinContainer.readContFromFile('bla.h5')

        self.cstore=cstore

        self.assertTrue( hasattr(cstore, 'hps1'),  "container storte does not contain hps1" )
        self.assertTrue( hasattr(cstore, 'hps2'),  "container storte does not contain hps2" )


    def test_slice(self):
        bh, bhflat = buildH1(self.ps1, xvalues, weights)
        content_3_1 = npa([1.2, 0. , 1. , 1.7, 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0.5])
        content_3_1_w2 = npa([1.44, 0.  , 1.  , 1.69, 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  ,0.25])

        bh.addAuxArray('bla', )
        bh.bla[:]=1
        
        psum = bh[sum,:]
        np.testing.assert_array_equal(psum.histoAt( (0,)).wcounts, npa([0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0.]), "psum 0 wrong ")
        np.testing.assert_array_equal(psum.histoAt( (1,)).wcounts, npa([1.2, 0.,  1.,  1.7, 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.5]), "psum 1 wrong ")
        np.testing.assert_array_equal(psum.histoAt( (2,)).wcounts, npa([0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0.,]), "psum 2 wrong")

        np.testing.assert_array_equal(psum.bla, np.array([5., 5., 5.], dtype=np.float32), 'psum wrong aux array ')
        
        #np.testing.assert_array_equal(bh[sum,sum].histoAt( (0,)).wcounts, npa([1.2, 0. , 2. , 1.7, 1. , 0. , 0. , 0. , 0. , 0. , 0. , 0.5]), "fullsum  wrong")
        np.testing.assert_array_equal(bh[sum,sum].wcounts, npa([1.2, 0. , 2. , 1.7, 1. , 0. , 0. , 0. , 0. , 0. , 0. , 0.5]), "fullsum  wrong")

        for j in range(3):
            partialsum = bh[sum,j].wcounts
            partialsum_ref = sum(bh.array[i,j] for i in range(5) )
            np.testing.assert_array_equal(partialsum_ref, partialsum, "partial sum wrong")
            
        
        self.hc = bh
t = TestHistoCont()
t.setUp()

unittest.main()
