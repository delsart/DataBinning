import unittest
import numpy as np
from DataBinning.PhaseSpace import PhaseSpace, rm
from DataBinning.Axis import AxisSpec


class TestPhaseSpace(unittest.TestCase):
    def setUp(self):
        self.ps1 = PhaseSpace("bla1",
                             AxisSpec("gen", title="xx",edges=np.array([0.,1,2,3,10, 12.5]) ,
                                      binNames = ["b0", "b1", "b2" , "b3", "b4"]),
                             AxisSpec("reg", title="xx", nbins=10, range=(0,1.), ),
                             AxisSpec("int", title="xx", nbins=10, )
                             )



        self.ps2 = PhaseSpace("PHSPACE",
                              AxisSpec("xax", title="xx",edges=np.array([0,1,2,3,10, 12.5]) ),
                              AxisSpec("yax", title="yy",edges=np.array([0,10, 15, 20]) ),
                              )
        

        

    def test_fill2(self):
        bin3_1_x=5.2
        bin3_1_y=13.0
        
        coords = [ np.array( [ 0.5,   3., 3.2,   3.3,bin3_1_x,bin3_1_x, 10,    -1, 17, 1.5]),
                   np.array( [ 1., 12.2 , 10.2, 11.3,bin3_1_y,bin3_1_y, 15,    1,  -1, 20.,]),
                  ]

        
        i, v = self.ps2.findBins( coords )


        expect_i = np.array([[ 0, 3, 3, 3, 3, 3, 4, -1, 5, 1],
                             [ 0, 1, 1, 1, 1, 1, 2,  0, -1, 3]], dtype=int
                            )
        expect_v = np.array( [ True, True, True, True, True, True, True, False, False, False] )

        np.testing.assert_array_equal( i, expect_i, "found indices not as expected" )
        np.testing.assert_array_equal( v, expect_v, "found validity not as expected" )


    def test_slice(self):

        subps = self.ps1[2:4,:,:5]

        self.assertEqual( subps.nDim() , 3 ,"wrong num dims for subps")
        self.assertEqual( subps.a0.nbins , 2 ,"wrong num dims for subps.a0")
        self.assertEqual( subps.a1.nbins , 10 ,"wrong num dims for subps.a1")
        self.assertEqual( subps.a2.nbins , 5 ,"wrong num dims for subps.a2")
        self.assertIs(subps.base, self.ps1, "wrong subps.base")

        subps2= self.ps1[:,rm,6:]
        self.assertEqual( subps2.nDim() , 2 ,"wrong num dims for subps2")
        self.assertEqual( subps2.shape() , (5,4) ,"wrong num dims for subps2")
        
        
        
# for interactive tests :
t = TestPhaseSpace()
t.setUp()
    
unittest.main()

