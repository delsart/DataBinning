import unittest
import numpy as np
from DataBinning.Histogram import Histo1D, Histo2D, HistoException
from DataBinning.Axis import AxisSpec



class TestHisto1(unittest.TestCase):
    def setUp(self):
        self.genaxis = AxisSpec("gen", title="xx",edges=np.array([0., 1, 2, 3, 10, 12.5]) ,
                                binNames = ["b0", "b1", "b2" , "b3", "b4"])
        self.regaxis = AxisSpec("reg", title="xx", nbins=10, range=(0,1.), )

        self.h1 = Histo1D( (self.regaxis.nbins, self.regaxis.range  ), name='hrange', title='h0'   )
        self.h1reg = Histo1D( xaxis=self.regaxis , name='hax', title='h0'  )
        self.h1gen = Histo1D( xaxis=self.genaxis , name='hax', title='h0'  )


    def test_base(self):
        self.assertEqual( self.h1.xaxis.nbins, self.regaxis.nbins, "h1 nbins differ from self.regaxis" )
        self.assertEqual( self.h1.wcounts.size, self.regaxis.nbins+2, "h1.wcounts.size differ from self.regaxis.nbins+2" )
        self.assertEqual( self.h1.xaxis.range, self.regaxis.range, "h1 range differ from self.regaxis" )

        np.testing.assert_array_equal( self.regaxis.edges, self.h1.xaxis.edges, "h1 edges differ from self.regaxis" )

        np.testing.assert_array_equal( self.h1reg.wcounts, self.h1.wcounts, "h1 wcounts differs from h1reg.wcounts" )


        
    def test_0fill(self):
        toBin = np.array([-3, 0, 0.02, 0.5, 0.66, 1., 1.3] )

        expectedW = np.array([1., 2., 0., 0., 0., 0., 1., 1., 0., 0., 0., 2.])
        self.h1.fill(toBin)
        self.h1reg.fill(toBin)

        np.testing.assert_array_equal( self.h1reg.wcounts, expectedW, "h1reg content differ from expected after 1st fill")
        np.testing.assert_array_equal( self.h1.wcounts, expectedW, "h1 content differ from expected after 1st fill")
        self.assertIs( self.h1.wcounts, self.h1.w2counts, "h1.wcounts  is not h1.w2counts after 1st fill")

        w = np.ones_like(toBin)*0.5
        expectedW =  np.array([1.5, 3., 0., 0., 0., 0., 1.5, 1.5, 0., 0., 0., 3.])
        self.h1.fill(toBin, w)
        self.h1reg.fill(toBin, w)

        expectedW2 = np.array([1.25, 2.5 , 0.  , 0.  , 0.  , 0.  , 1.25, 1.25, 0.  , 0.  , 0. ,2.5 ])
        np.testing.assert_array_equal( self.h1reg.wcounts, expectedW, "h1reg content differ from expected after 2nd fill")
        np.testing.assert_array_equal( self.h1.wcounts, expectedW, "h1 content differ from expected after 2nd fill")
        np.testing.assert_array_equal( self.h1.w2counts, expectedW2, "h1 w2counts differ from expected after 2nd fill")

        self.assertIsNot( self.h1.wcounts, self.h1.w2counts, "h1.wcounts still is h1.w2counts after 2nd fill")
        
        toBin = np.array([-3, 0, 0.02, 3.3 , 10.1, 12.9] )
        self.h1gen.fill(toBin)
        expectedW =  np.array([1., 2, 0,0,1,1,1])
        np.testing.assert_array_equal( self.h1gen.wcounts, expectedW, "h1gen content differ from expected after 1st fill")

        
        self.assertEqual( self.h1reg.binContentAt( 1 ), 3., "h1reg wrong content at bin 1")
        

    def test_slice(self):
        toBin = np.array([-3, 0, 0.02, 0.5, 0.66, 1., 1.3] )

        expectedW = np.array([1., 2., 0., 0., 0., 0., 1., 1., 0., 0., 0., 2.])
        self.h1.fill(toBin)

        self.assertEqual( self.h1[0], 1. , "h1[0] differ from 1.")
        self.assertEqual( self.h1[1], 2. , "h1[1] differ from 2.")
        self.assertEqual( self.h1[-0.2], 1. , "h1[-0.2] differ from 1.")
        self.assertEqual( self.h1[0.002], 2. , "h1[0.002] differ from 2.")

        h = self.h1[3:]
        np.testing.assert_array_equal( self.h1.wcounts[3-1:] , h.wcounts)        
        self.assertEqual( h[1], 0. , "h[1] differ from 0.")
        self.assertEqual( h[4], 1. , "h[4] differ from 1.")
        self.assertEqual( h.wcounts.size, h.xaxis.nbins+2 , "h.wcounts.size differ h.xaxis.nbins+2.")
        with self.assertRaises(HistoException, msg="h.fill did not raise"):
                h.fill( 0.5 ) 

        
        #np.testing.assert_array_equal( self.regaxis.histobinIndex( toBin ), np.array([ 0,  1,  1,  6,  7, 11, 11]), "regaxis.histobinIndex failed" )
        
# bps = BinnedPhaseSpace("BPS",
#                        AxisSpec("xax", title="xx",edges=np.array([0,1,2,3,10, 12.5]) ),
#                        AxisSpec("yax", title="yy",edges=np.array([0,10, 15, 20]) ),
#                        )


# bin3_1_x=5.2
# bin3_1_y=13.0

# coords = [ np.array( [ 0.5,   3., 3.2,   3.3,bin3_1_x,bin3_1_x, 10, ]),
#            np.array( [ 1., 12.2 , 10.2, 11.3,bin3_1_y,bin3_1_y, 15, ]),
# ]

# xrange= (0,10)
# yrange= (-1,1)
# svalues = np.array( [ 5. ,   10, 20, 100., 0.1 , 0.01,     3.1 ] )

# xvalues = np.array( [ 1.1 ,   1.1, 2.1, 2.1, -0.2, 10.2,     3.1 ] )
# yvalues = np.array( [ 0. ,   -0.1, .1, .1,  -1.2, 1.3,     3.1 ] )

# weights =np.array( [ 1.,     1. , 0.5, 1.2, 1.2  , 0.5,    1     ])



# def buildH1(values, weights):
#     bh = BinnedHistos( "hbps1", bps, (10,xrange))
#     bhflat = BinnedHistos( "hbps1Flat", bps, (10,xrange))



#     bh.fill( coords, values, weights )

#     binIndices = bps.buildNCoordinates(values.size)
#     flatInd, validCoordI = bps.findValidBins(coords, binIndices  , returnFlatI=True)
#     bhflat.fillAtIndices( values, validCoordI, flatInd, validW=weights)

#     return bh, bhflat

# def buildH2(values, weights):
#     bh = BinnedHistos2( "hbps2", bps, (10,xrange, 8,yrange))

    

#     bh.fill( coords, values, weights )

#     # binIndices = bps.buildNCoordinates(values.size)
#     # flatInd, validCoordI = bps.findValidBins(coords, binIndices  , returnFlatI=True)
#     # bhflat.fillAtIndices( values, validCoordI, flatInd, validW=weights)

#     return bh


# def buildScalar(values, weights):
#     bs = BinnedScalar( "hbps1", bps, )

#     bs.accumulate( coords, values,weights )

#     return bs


# ### ********************************************************
# ### ********************************************************
# if 1:
#     bh, bhflat = buildH1(xvalues, 1.)
#     content_3_1 = np.array([1., 0., 1., 2., 0., 0., 0., 0., 0., 0., 0., 1.], )

#     assert np.allclose(bh.histoAt( (3,1) ).hcontent , content_3_1), f"wrong content at (3,1)"
#     assert np.allclose(bh.array, bhflat.array), f"bh and bhflat differs"

#     print("fill ok")

#     bh, bhflat = buildH1(xvalues, weights)
#     content_3_1 = np.array([1.2, 0. , 1. , 1.7, 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0.5],)
#     content_3_1_w2 = np.array([1.44, 0.  , 1.  , 1.69, 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  ,0.25])

#     assert np.allclose(bh.histoAt( (3,1) ).hcontent , content_3_1), f"wrong Weight content at (3,1)"
#     assert np.allclose(bh.histoAt( (3,1) ).hw2 , content_3_1_w2), f"wrong  w2 at (3,1)"
#     assert np.allclose(bh.array,bhflat.array), f"weighted bh and bhflat differs"

#     print("fill w ok")





# ### ********************************************************
# ### ********************************************************
# if 1:
#     bs = buildScalar(svalues,1.)
#     scontent = np.array([[5.,0.,0.],
#                          [0.,0.,0.],
#                          [0.,0.,0.],
#                          [0.,130.11,0.],
#                          [0.,0.,3.1]],dtype=np.float32)

#     assert np.allclose( scontent, bs.array) , "wrong content in BinnedScalar"
#     scounts = np.array([[1., 0., 0.], [0., 0., 0.],[0., 0., 0.], [0., 5., 0.], [0., 0., 1.]], dtype=np.float32)
#     assert np.allclose( scounts, bs.counts) , "wrong counts in BinnedScalar"
#     print("fill scalar  ok")

#     bs = buildScalar(svalues,weights)
#     scontent = npa([[5.,0.,0.], [0.,0.,0.],[0.,0.,0.],[0.,140.125,0.], [0.,0.,3.1]])
#     assert np.allclose( scontent, bs.array) , "wrong content in weighted BinnedScalar"
#     scounts = npa([[1.,0.,0.],[0.,0.,0.],[0.,0.,0.],[0.,4.4,0.],[0.,0.,1.]])
#     assert np.allclose( scounts, bs.counts) , "wrong counts in BinnedScalar"
#     print("fill W scalar  ok")



# ### ********************************************************
# ### ********************************************************

# bh2 = buildH2(np.stack( [xvalues, yvalues]),1.)
# if 1 :
#     zs=[0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]
#     content_3_1 = np.array([[1., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
#                             zs, [0., 0., 0., 0., 1., 0., 0., 0., 0., 0.], [0., 0., 0., 0., 0., 2., 0., 0., 0., 0.], zs,zs, zs,zs,zs,zs,zs,[0., 0., 0., 0., 0., 0., 0., 0., 0., 1.]], dtype=np.float32)
#     assert np.allclose(bh2.histoAt( (3,1) ).hcontent , content_3_1), f"wrong histo2D content at (3,1)"
#     print("fill histo2d  ok")

#     bh2w = buildH2(np.stack( [xvalues, yvalues]),weights)
#     content_3_1_w =np.array([[1.2, 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0. ],
#                              zs,[0. , 0. , 0. , 0. , 1. , 0. , 0. , 0. , 0. , 0. ], [0. , 0. , 0. , 0. , 0. , 1.7, 0. , 0. , 0. , 0. ],
#                              zs,zs,zs,zs,zs,zs,zs,[0. , 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0. , 0.5]], dtype=np.float32)

#     assert np.allclose(bh2w.histoAt( (3,1) ).hcontent , content_3_1_w), f"wrong histo2D Weighted content at (3,1)"
#     print("fill histo2d weight  ok")

t = TestHisto1()
t.setUp()
unittest.main()
