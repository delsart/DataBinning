
from collections.abc import Mapping
from pickle import loads,dumps

from .Axis import AxisSpec
from .IndexUtils import addat

from numbers import Number, Integral

import numpy as np

globalPersistKey = '_BH_'

class HistoException(Exception):
    pass

class HistoBase:

    # used to refer to 'view' of histogram (ex: 1D slice of a 2D hist)
    base = None

    # can be overwritten by a dict of options passed to matplotlib.plot (see Graphics.HistoGraphics)
    drawStyle = None
    
    # What atttributes do we persistify 
    persistentAttr = ['name','title','wcounts', 'w2counts','drawStyle']
    persistPrefix = 'H0'
    
    def __init__(self,  hrange=None, x=None, y=None, w=None, name=None, title=None, xaxis=None, yaxis=None,
                 wcounts=None, w2counts=None,void=False):
        """ 
        Create a Histogram : 
          - binning set either :
            * with `hrange`  in the form (nbinsX,(lowX,highX)) or (nbinsX,(lowX,highX), nbinsY, (lowY,highY) )
            * with `xaxis` (an AxisSpec) and `yaxis` if Histo2D.
          - content : is x (and y, w) is given -> fills the histo with the x values. Else if wcounts is given adopt directly this array as the bin content

        same convention as ROOT : bin at i=0 is underflow, bin at i=nbins+1 is overflow.
        """

        if void : return
        name = name or (self.__class__.__name__)
        title = title or name+':xaxis:yaxis'
        self.name = name        
        self._initAxis(hrange,title,xaxis,yaxis )
        self._initContent(wcounts,w2counts,x,y,w)



    def persistName(self):
        return f'{globalPersistKey}{self.persistPrefix}_{self.name}'
    
    def saveInH5(self, fname, name=None,mode='a', ):
        from h5py import File
        if isinstance(fname,str):
            f = File(fname, mode)
        else: # should test it really is a h5py
            f =fname

        name = name or self.name
        gname = self.persistName()
        g = f.require_group(gname)

        self.saveTo(g)

        if isinstance(fname, str):
            f.close()


    def saveTo(self,g):
        for k in self.persistentAttr:
            v = getattr(self,k)
            if k in g : del(g[k])
            if v is not None:
                g[k]=v
                
        
    def loadFromH5(self, fname, loadContent=True, encoding=None):
        from h5py import File
        if isinstance(fname, str): f = File(fname, 'r')
        else: f=fname
        gname = self.persistName()
        g = f[gname]
        for k,v in g.items():
            v = v[()]
            if isinstance(v,bytes):
                try:
                    v=v.decode()
                except: pass
            setattr(self,k,v)
        self.postLoad()
        if isinstance(fname, str) : f.close()
        
    def postLoad(self):
        pass
    


    def scale(self, s,silent=False):
        self.wcounts *=s
        self.w2counts*=s*s
        if not silent and self.base :
            print(f"WARNING in Histo1D : scaling histo {self.name} but it is a VIEW on {self.base.name} -> base histo will be modified !")
        

    def values(self, flow=False):
        if flow:
            return self.wcounts
        else:
            return self.wcounts[self.sliceflow] # for ex [1:-1] in 1D
        
    def variances(self, flow=False):
        if flow:
            return self.w2counts
        else:
            return self.w2counts[self.sliceflow]

    def counts(self, flow = False):
        if self.w2counts is self.wcounts:
            counts = self.wcounts
        else:
            counts= np.divide(
                self.wcounts**2,
                self.w2counts,
                out=np.zeros_like(self.wcounts, dtype=np.float64),
                where=self.w2counts != 0)
        if flow:
            return counts
        else:
            return counts[self.sliceflow]



    def merge(self, h, alpha=1):
        if self.w2counts is self.wcounts and h.wcounts is h.w2counts:
            self.wcounts += h.wcounts*alpha
        else:
            self.w2counts = self.w2counts.copy()
            self.wcounts += h.wcounts*alpha
            self.w2counts += h.w2counts*alpha**2
            
    def reset(self):
        self.wcounts[:]=0
        self.w2counts[:]=0
        


class Histo1D(HistoBase):
    sliceflow=slice(1,-1,None)
    persistPrefix = 'H1'

    def clone(self):
        wcounts = self.wcounts.copy()
        w2counts = wcounts if (self.wcounts is self.w2counts) else self.w2counts.copy()
        h = Histo1D(name=self.name, title=self.title,
                    xaxis=self.xaxis.clone(), wcounts=wcounts, w2counts=w2counts)
        if self.drawStyle:
            h.drawStyle = dict(drawStyle)
        return h
    
    def _initAxis(self, hrange, title, xaxis, *_):
        titles = title.split(':')+['x']
        self.title = titles[0]
        if hrange is not None:
            xtitle=titles[1]
            if xaxis is not None :
                raise HistoException("Histo1D ERROR : either specify hrange OR xaxis, not both")
            if len(hrange)!=2:
                raise HistoException("Histo1D error : hrange must be in the form (nbins,(low,high)) ")
            self.xaxis = AxisSpec('xaxis',nbins=hrange[0], range=hrange[1],title=xtitle)
        elif xaxis is not None:
            self.xaxis = xaxis
        else:
            self.xaxis = AxisSpec('xaxis')

        self.axes = [self.xaxis] # compatibility with UHI
            
            
    def _initContent(self,wcounts,w2counts, x,y=None,w=None):
        if wcounts is not None:
            if x is not None:
                raise HistoException("Histo1D ERROR : do not specify x AND wcounts ")
            self.wcounts = wcounts
            if w2counts is None: w2counts = wcounts # trust the caller
            self.w2counts = w2counts
            return 

        # else we must instantiate the arrays :
        self.wcounts = np.zeros(self.xaxis.nbins+2)
        self.w2counts = self.wcounts # the default. Can be set to a proper array if w!=None in fill()

        if x is not None:            
            self.fill(x=x,w=w)
        
    def saveTo(self,g):
        super().saveTo(g)
        if 'xaxis' in g : del(g['xaxis'])
        g['xaxis']= np.bytes_(dumps(self.xaxis))

    def postLoad(self):
        self.xaxis = loads(self.xaxis.tobytes())

        
    def fill(self, x, w=None):
        if self.base :
            raise HistoException(f"Can not fill. Histo1 '{self.name}' is a view on '{self.base.name}' ")
        x_i = self.xaxis.histobinIndex(x)

        if isinstance(x_i,Integral):
            x_i=np.array([x_i], dtype=np.int64)

        # Deal with squared weights 
        if w is not None:
            if self.w2counts is self.wcounts:
                # we are given non unity weights. Create a new array
                self.w2counts = np.copy(self.w2counts)
            addat(self.w2counts, x_i, w*w)
        else:
            if self.w2counts is not self.wcounts:            
                addat(self.w2counts, x_i, 1.)
        # now fill weights/counts
        addat(self.wcounts, x_i, 1. if w is None else w)
        
        
    def findBin(self,x):
        return self.axis.histobinIndex(x)
        
    def binContentAt(self, i,  outC=None , scaleOutC=False):
        """returns the bin content at each i (that is the count or sum w in bin i)
        i is an index or array of indices  """
        if outC is None:
            outC=self.wcounts[i]
        else:
            if scaleOutC: outC *=self.wcounts[i]
            else: outC[:] = self.wcounts[i]
        return outC

    def sumW2ContentAt(self, i,  outC=None , scaleOutC=False):
        """returns the sum w2 in each bin content at index i
        i is an index or array of indices  """
        if outC is None:
            outC=self.w2counts[i]
        else:
            outC[:] = self.w2counts[i]
        return outC

    
    def binWidth(self, i=1):
        return self.xaxis.binWidth(i,histobin=True)
    
    def mean(self):
        c = self.xaxis.binCenter()
        return average( c, self.wcounts[1:-1])

    def meanAnderr(self):
        w = self.wcounts[1:-1]
        w2 = self.w2counts[1:-1]
        c = self.xaxis.binCenter()
        return averageAnderr(c, w, w2) 

    def nEffEntries(self):
        n = self.w2counts[1:-1].sum()
        return 0 if n==0. else self.wcounts[1:-1].sum()**2/n

    def normalize(self):
        s=self.wcounts[1:-1].sum()
        if s>0:
            self.scale(1/s)


    def __getitem__(self, s):

        # if a number or a str, just return the value of corresponding bin
        if isinstance(s,Number):
            if isinstance(s,Integral):
                return self.wcounts[ s]
            else:
                return self.wcounts[ self.xaxis.histobinIndex( s )  ]
        elif  isinstance(s,str):
            i=self.xaxis.nameIndex(s)
            return self.wcounts[ i ]
            
        # assume s is a slice and get a sub histo.
        start, stop, step = s.indices( self.xaxis.nbins+2 )
        if step != 1:
            raise HistoException("slicing histo with step !=1 not suported yet")
        newxaxis = self.xaxis.subaxis( slice(start-1, stop-1)  )
        newwcounts = self.wcounts[start-1:stop+1]
        neww2counts = self.w2counts[start-1:stop+1]

        h = Histo1D(name = self.name+f'{start}_{stop}',
                    title=  self.title,
                    xaxis = newxaxis, wcounts = newwcounts, w2counts = neww2counts)
        h.base = self
        return h

    

    def __repr__(self):
        return f"Histo1D {self.name}. "+self.xaxis._desc()

    

    
    
class Histo2D(HistoBase):
    sliceflow=(slice(1, -1, None), slice(1, -1, None))
    #save_att = HistoBase.save_att+[ 'ybins']        
    persistPrefix = 'H2'

    def clone(self):
        wcounts = self.wcounts.copy()
        w2counts = wcounts if (self.wcounts is self.w2counts) else self.w2counts.copy()
        h = Histo2D(name=self.name, title=self.title,
                    xaxis=self.xaxis.clone(),yaxis=self.yaxis.clone(), wcounts=wcounts, w2counts=w2counts)
        if self.drawStyle:
            h.drawStyle = dict(drawStyle)
        return h

    def _initAxis(self, hrange, title,xaxis, yaxis,  *_):
        titles = title.split(':')
        self.title = titles[0]
        if hrange is not None:
            titles += ['x','y',None][1-len(titles):-1] # just to complement titles to exactl 3 entries
            if xaxis is not None or yaxis is not None:
                raise HistoException("HistoBase ERROR : either specify hrange OR  (xaxis or yaxis), not both")
            if len(hrange)!=4:
                raise HistoException("Histo2D error : hrange must be in the form (nbinsX,(lowX,highX), nbinsY, (lowY,highY) ) ")
            self.xaxis = AxisSpec('xaxis',nbins=hrange[0], range=hrange[1],title=titles[1])
            self.yaxis = AxisSpec('yaxis',nbins=hrange[2], range=hrange[3],title=titles[2])
        elif xaxis is not None:
            self.xaxis = xaxis
            self.yaxis = yaxis
        else:
            self.xaxis = AxisSpec('xaxis')
            self.yaxis = AxisSpec('yaxis')

        self.axes = [self.xaxis,self.yaxis] # compatibility with UHI
            
            

    def _initContent(self,wcounts,w2counts, x,y=None,w=None):
        if wcounts is not None:
            if x is not None:
                raise HistoException("Histo2D ERROR : do not specify x AND wcounts ")
            self.wcounts = wcounts
            if w2counts is None: w2counts = wcounts # trust the caller
            self.w2counts = w2counts
            return 

        # else we must instantiate the arrays :
        self.wcounts = np.zeros( (self.xaxis.nbins+2, self.yaxis.nbins+2) )
        self.w2counts = self.wcounts # the default. Can be set to a proper array if w!=None in fill()

        if x is not None:            
            self.fill(x=x,y=y,w=w)


    def saveTo(self,g):
        super().saveTo(g)
        if 'xaxis' in g : del(g['xaxis'])
        g['xaxis']= np.bytes_(dumps(self.xaxis))
        if 'yaxis' in g : del(g['yaxis'])
        g['yaxis']=np.bytes_(dumps(self.yaxis))

    def postLoad(self):
        self.xaxis = loads(self.xaxis.tobytes())
        self.yaxis = loads(self.yaxis.tobytes())

    
    def fill(self, x, y, w=None):
        from numbers import Number, Integral

        if self.base :
            raise HistoException(f"Can not fill. Histo2 '{self.name}' is a view on '{self.base.name}' ")
        x_i = self.xaxis.histobinIndex(x)
        y_i = self.yaxis.histobinIndex(y)

        if isinstance(x_i,Integral):
            x_i=np.array([x_i], dtype=np.int64)
        if isinstance(y_i,Integral):
            y_i=np.array([y_i], dtype=np.int64)

        ii = np.ravel_multi_index( (x_i,y_i), self.wcounts.shape)

        # Deal with squared weights 
        if w is not None:
            if self.w2counts is self.wcounts:
                # we are given non unity weights. Create a new array
                self.w2counts = np.copy(self.w2counts)
            addat(self.w2counts.ravel(), ii, w*w)
        else:
            if self.w2counts is not self.wcounts:            
                addat(self.w2counts.ravel(), ii, 1.)
        # now fill weights/counts
        addat(self.wcounts.ravel(), ii, 1. if w is None else w)


    def findBin(self,x,y):
        raise NotImplentedError("Histo2D.findBin")

        
    def binContentAt(self, x, y, outC=None , _x_i=None, _y_i=None):
        pass


    def __getitem__(self, s):
        # if a number , just return the value of corresponding flat bin
        if isinstance(s,Integral):
            return self.wcounts.ravel()[s]
        # it must be a tuple:
        i,j = s
        if isinstance(i,Integral) and isinstance(j,Integral):
            return self.wcounts[i,j]

        def axisAndslice(ax, i):
            issum = False
            if isinstance(i,slice):                
                istart = max((i.start or 0)-1, 0)
                istop = min(  (i.stop or ax.nbins+2)+1, ax.nbins+2)
                step = i.step
                if step is sum:
                    i= slice(istart+1, istop-1) # we're going to sum on this slice, no need to include overbins
                    
                    newxaxis = None
                    issum = True
                elif step not in [1,None]:
                    raise HistoException("slicing histo with step !=1 not suported yet")
                else:
                    i= slice(istart, istop) # redefine the slice                                    
                    if istart==0 and istop==(ax.nbins+2):
                        newxaxis = ax
                    else:
                        axslice = slice(istart,istop-2)
                        newxaxis = ax.subaxis( axslice  )
                
            else:
                if i is sum:
                    issum = True
                    i = slice(None)                                    
                newxaxis = None
            return newxaxis, i, issum

        newxaxis, i,sumx = axisAndslice(self.xaxis, i)
        newyaxis, j,sumy = axisAndslice(self.yaxis, j)

        # print(newxaxis, i)
        # print(newyaxis, j)
        
        if newyaxis and newxaxis:
            h = Histo2D(name=self.name+"_sub", xaxis=newxaxis, yaxis=newyaxis,title=self.title,
                         wcounts = self.wcounts[i,j],
                         w2counts = self.w2counts[i,j],)
        elif newxaxis:
            w = self.wcounts[i,j] if not sumy else self.wcounts[i,j].sum(axis=1)
            w2 = self.w2counts[i,j] if not sumy else self.w2counts[i,j].sum(axis=1)
            h = Histo1D(name=self.name+"_projX", xaxis=newxaxis, title=self.title,
                        wcounts = w, w2counts = w2)
        elif newyaxis:
            w = self.wcounts[i,j] if not sumx else self.wcounts[i,j].sum(axis=0)
            w2 = self.w2counts[i,j] if not sumx else self.w2counts[i,j].sum(axis=0)
            h = Histo1D(name=self.name+"_projY", xaxis=newyaxis, title=self.title,
                        wcounts = w, w2counts = w2)
        elif sumx and sumy:
            return self.wcounts.sum()
        elif sumx:
            return self.wcounts[:,j].sum()
        elif sumy:
            return self.wcounts[i,:].sum()
        
        h.base = self
        return h

        

def loadManyHistos(fname, outtype=dict):
    from h5py import File
    
    content = File(fname)
    out = outtype()
    hkey = f'{globalPersistKey}'
    for k,hdic in content.items():
        if k.startswith(hkey):
            _,_,htype, *r=k.split('_')            
            hname = '_'.join(r)
            htype = dict(H1=Histo1D,H2=Histo2D)[htype]
            h = htype(void=True)
            h.name= hname
            h.loadFromH5(content)
            out[hname] = h
    return out


def average(x, w):
    wsum = w.sum()
    if wsum==0.:
        return 0.
    return (w*x).sum()/wsum

def averageAnderr(x,w,w2):
    wsum = w.sum()
    if wsum==0.:
        return 0, 1.
    over_neff =  w2.sum() / (wsum*wsum)
    m = (w*x).sum()/wsum
    err = np.sqrt( ( (x*x*w).sum()/wsum -m*m )*over_neff)

    return m ,err
        
