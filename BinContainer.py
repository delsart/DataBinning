import h5py
import numpy as np
from .PhaseSpace import PhaseSpace
from .Histogram import globalPersistKey
import types


class BinContainer:
    """Base class to store numpy arrays in a multidimensionnal phase space
    The phase space is represented by a PhaseSpace  (self.phspace attribute)
    Concrete class will implement the actual content. Typically by implementing a numpy array 
    of dimension self.phspace.nDim()+1 where the last dimension represent the arrays in each bin.
    """

    drawStyle = {} # option to be passed to matplotlib graphs.
    isLoadedFromInput = False

    persistentAttr = []   # list of attributes of this class to save out
    persistentAttrHeavy = [ ] # list of heavy attributes(typically the content arrays) to save out. Not loaded by default for perf reasons.

    persistNonArrays = [] # list of non-array attribute we want to save.
    persistPrefix = 'BC0' # this is a compact identifier for this class. Used in persistant keys when saving to h5 files.  

    # user may associate their own arrays as attributes to this Container. Listing the attributes name in auxArrayNames will make them saved/loaded with
    # this container. It will also make them transfered when slicing the container.
    auxArrayNames = [] 
    
    inputFile = None
    
    additionalSetup = []  # A list of functions called by setup(). This allows clients (or, for ex, graphics system) to customize the setup of the containe.

    numInnerDim = 0

    def __init__(self, name, phspace, initArrays=True, label=None):
        self.name = name
        self.phspace = phspace

        if initArrays:
            self.initArrays()

        self.label = label or name
        self.auxArrayNames = []
        self.persistNonArrays = []
        
    def setup(self):
        for f in self.additionalSetup:
            f(self)

    
    def nameAt(self, coords):
        return self.name+'_'+self.phspace.tagAt(coords)

    def addAuxArray(self, name, shape=(), dtype=np.float32, array=None, ):
        """This creates a new array and sets it as an attribute to self and records it as an auxilliary array.

        This is is typically useful to store calculations made for each bin of this container.

        The full shape of the created array will be : self.phspace.shape()+shape

        returns the created array.
        """
        a = array if array is not None else np.zeros( self.phspace.shape()+shape, dtype=dtype)
        self.auxArrayNames.append(name)
        setattr(self, name, a)
        return a
        
    def persName(self):
        return globalPersistKey+self.persistPrefix+'_'+self.name         

    def saveInH5(self, fname=None, mode='a', ):

        fname = fname or self.inputFile
        if isinstance(fname, str):
            f = h5py.File(fname, mode)
        else: f = fname
        gname = self.persName()
        g = f.require_group(gname)
        from pickle import dumps
        phspace_s=dumps(self.phspace)
        
        if 'phspace' in g:del(g['phspace'])
        g['phspace'] =np.bytes_(phspace_s)


        if 'persistNonArrays' in g: del(g['persistNonArrays'])
        g['persistNonArrays'] = np.bytes_( dumps( dict( (n,getattr(self,n))  for n in self.persistNonArrays+['auxArrayNames']) ) )
        
        for k in self.persistentAttr+self.persistentAttrHeavy+self.auxArrayNames:
            v = getattr(self,k)
            if k in g : del(g[k])
            if v is not None:
                g[k]=v

        if isinstance(fname, str):
            f.close()
        
        
    def loadFromH5(self, fname, loadContent=True, encoding=None):
        if isinstance(fname, str): f = h5py.File(fname, 'r')
        else: f=fname
        gname = self.persName()
        #print( gname , ' .. ', f)
        g = f[gname]
        self.inputFile = f.filename
        
        from pickle import loads
        opt={} if encoding is None else dict(encoding=encoding)
        #print(opt, encoding,  g['phspace'][()].tostring())
        self.phspace = loads( g['phspace'][()].tobytes()  , **opt)
        self.phspace.indices = PhaseSpace.Indices(self.phspace)
        #d = loads( g['persistNonArrays'][()].tostring()  , **opt)
        d = loads( g['persistNonArrays'][()].tobytes()  , **opt)
        self.persistNonArrays = []
        for k,v in d.items():
            setattr(self,k,v)
            self.persistNonArrays+=[k]

        dontLoadYet = self.persistentAttrHeavy+self.auxArrayNames+['persistNonArrays']
        for k,v in g.items():
            if k in dontLoadYet or k=='phspace' : continue
            v = v[()]
            if isinstance(v,bytes):
                try:
                    v=v.decode()
                except: pass
            setattr(self,k,v)
            
        if loadContent : self.loadContent(f)
        if isinstance(fname, str) : f.close()

    def loadContent(self,f=None):
        """Load the heavy arrays for this BinContainer from file (if f==None, default to self.inputFile) """
        if self.isLoadedFromInput:
            return

        f = self.inputFile if f is None else f
        if isinstance(f,str): f =  h5py.File(f, 'r')            
        gname = self.persName()
        g = f[gname]
        for k in self.persistentAttrHeavy+self.auxArrayNames:
            setattr(self,k, g[k][()])
        self.isLoadedFromInput = True
        if isinstance(f,str) : f.close()


    def applyFunc(self, func, rangei=() ):
        """Apply the function func on some bin range (default : all bins in the phase space)
        Function is called for wach bin as : func( coords, self) 
        """
        for c in self.phspace.indices[rangei]:
            func( tuple(c), self)
        



    def reduceToCurveContainer(self, curvDim, reduceFunc,name=None, withErr=True, ylabel=None, graphStyle={}):
        """Build a new CurveContainer for a subspace of self.phspace with dimension curvDim removed. 

        Typically if self's  space has dim (I,J,K) then 'reduceToCurveContainer(2, f )' will return a 
        CurveContainer with phase space (I,J) and each bin (i,j) containing a graph calculated by f from all data
        at bins (i,j,:) from self.

        The graphs/curves in the new CurveContainer are of the form (X,Y) where 
          * X and Y are arrays of same size (or less) as dimension curvDim 
          * X and Y are calculated by calling 'reduceFunc' on each bin of the new phase space.         

        For each bin coordinates c of the new phase space, 'reduceFunc()' is called as 

          reduceFunc(self, coordinates_at_c, X) and is expected to return ( X,Y,Y_err )


        Where X=array of positions of bins along curvDim and 
        coordinates_at_c is the slice of all indices in the top phase space (self.phspace)  
        "above" indices c in the subspace being created. It's a tuple of int arrays (== a numpy slice)
        
        if c=(c_0,...,c_n) then coordinates_at_c=(c_0,...,:,..,c_n) where ':' is at pos curvDim

        Ex : if we have dimensions (3,5,6)  and reducing curvDim=1 then each coordinates_at_c will correspond to 5 indices.

        the result (X,Y,Y_err) may have a size < (nbins in linedDim)
        """
        from .CurveContainer import CurveContainer
        # create a new CurveContainer 
        name = name or self.name+'_lines_'+'_'.join([str(i) for i in range(self.phspace.nDim()) if i!=curvDim])
        newphspace = self.phspace.subspace(removeDim=curvDim)
        
        curvCont = CurveContainer(name, newphspace, maxN = self.phspace.axis[curvDim].nbins, withErr=withErr,
                                  xlabel = self.phspace.axis[curvDim].title,
                                  ylabel = ylabel or reduceFunc.__name__ )
        curvCont.drawStyle = dict(self.drawStyle)
        curvCont.drawStyle.update(graphStyle)
        curvCont.parent = self
        # --

        # a shortcut for a slice spanning a full dimension
        all = slice(None)

        # shortcut to our index utility
        topindices = self.phspace.indices

        # create the default X of the graphs/lines to be created
        edges = self.phspace.axis[curvDim].edges
        x0 = 0.5*(edges[:-1]+edges[1:])

        # loop over new binned space and calculate the lines :
        for c in newphspace.indices[()]:
            #print(c)
            # we build all the indices in our space corresponding to c :
            topslice=tuple(c[:curvDim])+(all,)+tuple(c[curvDim:])
            c_top = tuple(topindices[topslice].T)

            # call the reduceFunc :
            x,y,y_err=reduceFunc(self, c_top, x0)

            # assign results to the new CurveContainer 
            c=tuple(c)
            n=x.shape[0] # it is possible x !=x0 and some 'invalid' points have been ignored. We honour this.
            curvCont.array_x[c][:n]=x
            curvCont.array_y[c][:n]=y
            curvCont.npoints[c]=n
            if withErr: curvCont.array_yerr[c][:n]=y_err

        return curvCont
        

    def __repr__(self):
        return f"{self.__class__.__name__} '{self.name}'\n{self.phspace}"
    


    ##*******************************************************
    
    knownClasses= dict(  )
    @staticmethod
    def readContFromFile(fname, store=None, loadContent=False, encoding=None, exclude='', tag=''):
        """read all instances of BinContainer in a h5 file and assign them to a 'store' (by default a simple namespace).
        The content of the BinContainer is not read by default.

        """
        if isinstance(fname, str): f = h5py.File(fname, 'r')
        else: f=fname

        store = store or types.SimpleNamespace()
        allBA = []

        print( tag, '  ---- Reading from ', fname)
        for k in f.keys():
            if exclude!='' and exclude in k: continue
            print(k)
            if k =='trainingSummary':
                from pickle import loads
                setattr(store, 'trainingSummary'+tag,  loads( f[k][()].tostring()  ) )
            
            if k.startswith('_BH_'):
                persistPrefix = k[4:7]
                name = k[8:]
                klass = BinContainer.knownClasses[ persistPrefix ]
                o = klass( name , phspace=None , initArrays=False)
                o.loadFromH5(f, loadContent=loadContent, encoding=encoding)
                o.label = tag
                o.setup()
                setattr(store, name+tag, o)
                allBA.append(o)
        f.close()
        return store, allBA

    @staticmethod
    def toKnownClasses(cls):
        """Used as a decoration on derived classes, just to record their persistPrefix """
        BinContainer.knownClasses[cls.persistPrefix] = cls
        return cls



